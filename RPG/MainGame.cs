﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG
{
    class MainGame
    {
        string answer;
        Hero myHero;
        Monster myMonster;
        public MainGame()
        {
             myHero = new Hero();
             myMonster = new Monster();
            Hero.Initialize(myHero);
            

            BasicLoop();
        }
        void BasicLoop()
        {
            do
            {
                Battle b = new Battle(myHero,myMonster);
                Console.WriteLine("Do you want to play again?");
                answer = Console.ReadLine();
            }
            while (answer == "Yes" || answer == "yes" || answer == "Y" || answer == "y");

        }

        //public static string RandomNextPerson()
        //{
        //    Random r = new Random();
        //    int random = 0;
        //    string randomPerson;
        //    r.Next(1, 5);
        //    switch (random)
        //    {
        //        case 1:
        //            randomPerson = "Slime";
        //            break;
        //        case 2:
        //            randomPerson = "Mage";
        //            break;
        //        case 3:
        //            randomPerson = "Monster";
        //            break;
        //        case 4:
        //            randomPerson = "Rouge";
        //            break;
        //        default:
        //            randomPerson = "Monster";
        //            break;

        //    }
        //    return randomPerson;
        //}
    }
   

}
