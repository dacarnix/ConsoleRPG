﻿using System;

namespace RPG
{
    class Reptile : Characters
    {
        public Reptile()
        {
             
            base.AiSpell = 30;
            base.AiAttack = 40;
            base.AiDefend = 80;
            base.CurrentHealth = 1000;
            base.MaxHealth = 1000;
            base.CurrentMagic = 0;
            base.MaxMagic = 0;
            base.Strength = 250;
            base.Defense = 80;
            base.Agility = 45;
            base.Intelligence = 20;
            base.Experience = OutputHelper.rand.Next(50, 70);
            base.Gold = OutputHelper.rand.Next(100, 130);
            base.Identifier = "Reptile";
            base.isNamed = true;
            base.isAlive = true;
            base.AttackDamage = Strength;
        }

        public override string AI()
        {
             
            string AiChoice;
            byte AiChoiceMaker = ((byte)(OutputHelper.rand.Next(1, 100)));
            if (AiChoiceMaker < base.AiAttack) AiChoice = "A";
            else if (AiChoiceMaker >= base.AiAttack && AiChoiceMaker <= base.AiDefend) AiChoice = "D";
            else if (AiChoiceMaker > base.AiSpell && AiChoiceMaker < base.AiDefend) AiChoice = "A";
            else AiChoice = "A";
            return AiChoice;

        }
    }
}
