﻿using System;

namespace RPG
{
    class ErrorMacro : Characters
    {
        public ErrorMacro()
        {
             
            base.AiSpell = 0;
            base.AiAttack = 80;
            base.AiDefend = 100;
            base.CurrentHealth = 400;
            base.MaxHealth = 400;
            base.CurrentMagic = 0;
            base.MaxMagic = 0;
            base.Strength = 160;
            base.Defense = 70;
            base.Agility = 50;
            base.Experience = OutputHelper.rand.Next(1, 30);
            base.Gold = OutputHelper.rand.Next(2, 20);
            base.Identifier = "Error Macro";
            base.isAlive = true;
            base.isNamed = true;
            base.AttackDamage = Strength;
        }

        public override string AI()
        {
             
            string AiChoice;
            byte AiChoiceMaker = ((byte)(OutputHelper.rand.Next(1, 100)));
            if (AiChoiceMaker < base.AiAttack) AiChoice = "A";
            else if (AiChoiceMaker >= base.AiAttack && AiChoiceMaker <= base.AiDefend) AiChoice = "D";
            else if (AiChoiceMaker > base.AiSpell && AiChoiceMaker < base.AiDefend) AiChoice = "S";
            else AiChoice = "A";
            return AiChoice;

        }
    }
}
