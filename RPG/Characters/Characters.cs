﻿using System;

namespace RPG
{
    public class Characters 
    {
        public bool defending, increaseAttack,spellDefending;
        protected Random rand;
        public bool Boss1Defeated, Boss2Defeated,Boss3Defeated;
        public bool GameWon;
        public int HighScore;
        public int IntelBuff, StrengthBuff, AgilityBuff,GoldBuff,HealthBuff;
        public int HealthPotions, ManaPotions;
        protected int AiAttack, AiDefend, AiSpell;
        public int CurrentHealth, MaxHealth, CurrentMagic;
        public int MaxMagic, Strength, Defense, Agility, Intelligence;
        public int Experience, Gold, AttackDamage;
        public string Identifier;
        public bool isAlive;
        public bool roamingKey, isRoaming,secretVillage;
        public bool isNamed = false;
        protected int spellOne, spellTwo, spellThree;

        public bool fled;

        public Characters()
        {
            spellOne = 0;
            spellTwo = 0;
            spellThree = 0;
            defending = false;
            increaseAttack = false;
            spellDefending = false;
            fled = false;
        }

        public virtual string AI()
        {
            string choice = "";
            return choice;
        }

        public virtual string SpellAI()
        {
            string choice = "F";
            int aiSpellChoice;

            aiSpellChoice = OutputHelper.rand.Next(1, 100);
            if (this.CurrentHealth != this.MaxHealth)
            {

                if (this.spellOne <= aiSpellChoice && CurrentMagic >= 50) choice = "F";
                else if (aiSpellChoice <= this.spellTwo && aiSpellChoice >= this.spellOne && CurrentMagic >= 40) choice = "I";
                else choice = "H";
            }
            else if (this.CurrentHealth == this.MaxHealth)
            {
                if (this.spellOne <= aiSpellChoice && CurrentMagic >= 50) choice = "F";
                else if (aiSpellChoice <= this.spellTwo && aiSpellChoice >= this.spellOne && CurrentMagic >= 40) choice = "I";
                else choice = "I";
                return choice;
            }
            return choice;
        }
    }

}
