﻿using System;

namespace RPG
{
    class NightRouge : Characters
    {
        public NightRouge(Hero hero)
        {
             
            base.AiSpell = 0;
            base.AiAttack = 80;
            base.AiDefend = 100;
            base.CurrentHealth = 160 + (hero.Level * 10);
            base.MaxHealth = 160 + (hero.Level * 10);
            base.CurrentMagic = 0;
            base.MaxMagic = 0;
            base.Strength = 80 + (hero.Level * 5);
            base.Defense = 20 + (hero.Level * 5);
            base.Agility = 5 + (hero.Level * 5);
            base.Experience = OutputHelper.rand.Next((4 + (hero.Level)), (10 + (hero.Level )));
            base.Gold = OutputHelper.rand.Next((4 + (hero.Level)), (13 + (hero.Level)));
            base.Identifier = "Night Rouge";
            base.isAlive = true;
            base.AttackDamage = Strength;
        }

        public override string AI()
        {
             
            string AiChoice;
            byte AiChoiceMaker = ((byte)(OutputHelper.rand.Next(1, 100)));
            if (AiChoiceMaker < base.AiAttack) AiChoice = "A";
            else if (AiChoiceMaker >= base.AiAttack && AiChoiceMaker <= base.AiDefend) AiChoice = "D";
            else if (AiChoiceMaker > base.AiSpell && AiChoiceMaker < base.AiDefend) AiChoice = "S";
            else AiChoice = "A";
            return AiChoice;

        }
    }
}
