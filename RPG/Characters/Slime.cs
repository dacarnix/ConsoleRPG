﻿using System;

namespace RPG
{
    class Slime:Characters
    {
        public Slime()
        {
             
            base.AiSpell = 0;
            base.AiAttack = 80;
            base.AiDefend = 100;
            base.CurrentHealth = 150;
            base.MaxHealth = 150;
            base.CurrentMagic = 0;
            base.MaxMagic = 0;
            base.Strength = 60;
            base.Defense = 30;
            base.Agility = 10;
            base.Experience = OutputHelper.rand.Next(4,9);
            base.Gold = OutputHelper.rand.Next(5,10);
            int name = OutputHelper.rand.Next(1, 4);
            if (name == 1) base.Identifier = "Green Slime";
            if (name == 2) base.Identifier = "Red Slime";
            if (name == 3) base.Identifier = "Blue Slime";
            if (name == 4) base.Identifier = "Black Slime";

            base.isAlive = true;
            base.AttackDamage = Strength;
        }

        public override string  AI()
        {
             
            string AiChoice;
            byte AiChoiceMaker = ((byte)(OutputHelper.rand.Next(1,100)));
            if (AiChoiceMaker < base.AiAttack) AiChoice = "A";
            else if (AiChoiceMaker >= base.AiAttack && AiChoiceMaker <= base.AiDefend) AiChoice = "D";
            else if (AiChoiceMaker > base.AiSpell && AiChoiceMaker < base.AiDefend) AiChoice = "S";
            else AiChoice = "A";
            return AiChoice;

        }
    }
}
