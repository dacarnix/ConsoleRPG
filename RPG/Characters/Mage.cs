﻿using System;

namespace RPG
{
    class Mage:Characters
    {
       public Mage()
        {
             
            base.AiSpell =50;
            base.AiAttack = 95;
            base.AiDefend = 65;
            base.CurrentHealth = 120;
            base.MaxHealth = 120;
            base.CurrentMagic = 200;
            base.MaxMagic = 200;
            base.Strength = 50;
            base.Defense = 30;
            base.Agility = 40;
            base.Intelligence = 5;
            base.Experience = OutputHelper.rand.Next(4,12);
            base.Gold = OutputHelper.rand.Next(4,10);
            base.Identifier = "Mage";
            base.isAlive = true;
            base.AttackDamage = Strength;
            base.spellOne = 40;
            base.spellTwo = 80;

        }

        public override string  AI()
        {
             
            string AiChoice;
            byte AiChoiceMaker = ((byte)(OutputHelper.rand.Next(1,100)));
            if (AiChoiceMaker <= base.AiAttack && AiChoiceMaker >= base.AiDefend) AiChoice = "A";
            else if (AiChoiceMaker > base.AiSpell&& AiChoiceMaker <= base.AiDefend) AiChoice = "D";
            else if (AiChoiceMaker <= base.AiSpell && AiChoiceMaker >= 1 && CurrentMagic >= (base.CurrentHealth == base.MaxHealth ? 40 : 30)) AiChoice = "S";
            else AiChoice = "S";
            return AiChoice;

        }
    }
}
