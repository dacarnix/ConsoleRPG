﻿using System;

namespace RPG
{
    class Rouge:Characters
    {
         public Rouge()
        {
             
            base.AiSpell = 0;
            base.AiAttack = 80;
            base.AiDefend = 100;
            base.CurrentHealth = 120;
            base.MaxHealth = 120;
            base.CurrentMagic = 0;
            base.MaxMagic = 0;
            base.Strength = 85;
            base.Defense = 20;
            base.Agility = 10;
            base.Experience = OutputHelper.rand.Next(8,10);
            base.Gold = OutputHelper.rand.Next(4, 9);
            base.Identifier = "Rogue";
            base.isAlive = true;
            base.AttackDamage = Strength;
        }

        public override string  AI()
        {
             
            string AiChoice;
            byte AiChoiceMaker = ((byte)(OutputHelper.rand.Next(1,100)));
            if (AiChoiceMaker < base.AiAttack) AiChoice = "A";
            else if (AiChoiceMaker >= base.AiAttack && AiChoiceMaker <= base.AiDefend) AiChoice = "D";
            else if (AiChoiceMaker > base.AiSpell && AiChoiceMaker < base.AiDefend) AiChoice = "S";
            else AiChoice = "A";
            return AiChoice;

        }
    }
}
