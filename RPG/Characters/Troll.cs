﻿using System;

namespace RPG
{
    class Troll : Characters
    {
        public Troll(Hero hero)
        {
             
            base.AiSpell = 0;
            base.AiAttack = 80;
            base.AiDefend = 100;
            base.CurrentHealth = 180 + (hero.Level * 7);
            base.MaxHealth = 180 + (hero.Level * 7);
            base.CurrentMagic = 0;
            base.MaxMagic = 0;
            base.Strength = 70 + (hero.Level * 7);
            base.Defense = 40 + (hero.Level * 5);
            base.Agility = 20 + (hero.Level * 7);
            base.Experience = OutputHelper.rand.Next((4 + (hero.Level)), (11 + (hero.Level)));
            base.Gold = OutputHelper.rand.Next((7 + (hero.Level)), (11 + (hero.Level)));
            base.Identifier = "Troll";
            base.isAlive = true;
            base.AttackDamage = Strength;
        }

        public override string AI()
        {
             
            string AiChoice;
            byte AiChoiceMaker = ((byte)(OutputHelper.rand.Next(1, 100)));
            if (AiChoiceMaker < base.AiAttack) AiChoice = "A";
            else if (AiChoiceMaker >= base.AiAttack && AiChoiceMaker <= base.AiDefend) AiChoice = "D";
            else if (AiChoiceMaker > base.AiSpell && AiChoiceMaker < base.AiDefend) AiChoice = "S";
            else AiChoice = "A";
            return AiChoice;

        }
    }
}
