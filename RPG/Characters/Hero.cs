﻿using System;
using System.Collections.Generic;

namespace RPG
{
    public class Hero : Characters
    {
        public List<string> Helmets;
        public List<string> Plates;
        public List<string> Leggings;
        public List<string> Weapons;
        public int Level, WeaponAttack;
        public int HelmetDefense, ChestDefense, LegDefense;
        public int SkillPoints;
        public string savePoint = "village";
        public string Helmet, Plate, Legging, Weapon;
        public Hero()
        {
            Helmets = new List<string>();
            Plates = new List<string>();
            Leggings = new List<string>();
            Weapons = new List<string>();
            isNamed = true;
        }

        public static void Initialize(Hero hero)
        {
            hero.LegDefense = hero.ChestDefense = hero.HelmetDefense = 0;
            hero.Level = 1;
            hero.Boss1Defeated = false;
            hero.Boss2Defeated = false;
            hero.HealthBuff = 0;
            hero.HighScore = 0;
            hero.GoldBuff = 0;
            hero.GameWon = false;
            hero.IntelBuff = 0;
            hero.roamingKey = false;
            hero.StrengthBuff = 0;
            hero.AgilityBuff = 0;
            hero.Helmet = "";
            hero.Legging = "";
            hero.Plate = "";
            hero.Weapon = "";
            hero.SkillPoints = 0;
            hero.CurrentMagic = hero.CurrentHealth = 0;

            hero.Strength = 10;
            hero.Intelligence = 5;
            hero.Agility = 7;

            BattleHelper.RefreshStats(hero);
            hero.CurrentMagic = hero.MaxMagic;
            hero.CurrentHealth = hero.MaxHealth;

            hero.Experience = 0;
            hero.Gold = 15;
            while (hero.Identifier == null || hero.Identifier.Trim() == "" ||
                hero.Identifier == " ")
            {
                Console.WriteLine("What is your Hero's name?");
                hero.Identifier = Console.ReadLine();
                for (int i = 0; i < 10; i++)
                {
                    if (hero.Identifier.Contains(i.ToString()))
                    {
                        Console.WriteLine();
                        Console.WriteLine("Please only enter letters");
                        BattleHelper.Anykey();
                        hero.Identifier = "";
                        Hero.Initialize(hero);
                    }
                }   
            }
            hero.isAlive = true;
        }
    }

}
