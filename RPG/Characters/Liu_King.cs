﻿using System;

namespace RPG
{
    class Liu_King : Characters
    {
        public Liu_King(Hero hero)
        {
             
            base.AiSpell = 65;
            base.AiAttack = 40;
            base.AiDefend = 80;
            base.CurrentHealth = 1000 + (hero.Level * 25);
            base.MaxHealth = 1000 + (hero.Level * 25);
            base.CurrentMagic = 30;
            base.MaxMagic = 30;
            base.Strength = 230 + (hero.Level * 2);
            base.Defense = 60 + (hero.Level / 2);
            base.Agility = 50;
            base.Intelligence = 15;
            base.Experience = OutputHelper.rand.Next(50, 70);
            base.Gold = OutputHelper.rand.Next(100, 130);
            base.Identifier = "Liu King";
            base.isNamed = true;
            base.isAlive = true;
            base.AttackDamage = Strength;
        }

        public override string AI()
        {
             
            string AiChoice;
            byte AiChoiceMaker = ((byte)(OutputHelper.rand.Next(1, 100)));
            if (AiChoiceMaker < base.AiAttack) AiChoice = "A";
            else if (AiChoiceMaker > base.AiSpell && AiChoiceMaker < base.AiDefend && CurrentMagic >= (base.CurrentHealth == base.MaxHealth ? 40 : 30)) AiChoice = "S";
            else if (AiChoiceMaker >= base.AiAttack && AiChoiceMaker <= base.AiDefend) AiChoice = "D";
            else AiChoice = "A";
            return AiChoice;
        }
    }
}
