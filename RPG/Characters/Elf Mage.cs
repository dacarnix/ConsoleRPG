﻿using System;

namespace RPG
{
    class ElfMage : Characters
    {
        public ElfMage(Hero hero)
        {
            base.AiSpell = 50;
            base.AiAttack = 95;
            base.AiDefend = 65;
            base.CurrentHealth = 150 + (hero.Level * 5);
            base.MaxHealth = 150 + (hero.Level * 5);
            base.CurrentMagic = 140;
            base.MaxMagic = 140;
            base.Strength = 40 + (hero.Level * 5);
            base.Defense = 30 + (hero.Level * 5);
            base.Agility = 5 + (hero.Level * 5);
            base.Intelligence = 5 + (int)(hero.Level / 2);
            base.Experience = OutputHelper.rand.Next((5 + (hero.Level )), (15 + (hero.Level)));
            base.Gold = OutputHelper.rand.Next((12 + (hero.Level)), (17 + (hero.Level)));
            base.Identifier = "Elf Mage";
            base.isAlive = true;
            base.AttackDamage = Strength;
            base.spellOne = 40;
            base.spellTwo = 80;

        }

        public override string AI()
        {
             
            string AiChoice;
            byte AiChoiceMaker = ((byte)(OutputHelper.rand.Next(1, 100)));
            if (AiChoiceMaker <= base.AiAttack && AiChoiceMaker >= base.AiDefend) AiChoice = "A";
            else if (AiChoiceMaker > base.AiSpell && AiChoiceMaker <= base.AiDefend) AiChoice = "D";
            else if (AiChoiceMaker <= base.AiSpell && AiChoiceMaker >= 1 && CurrentMagic >= (base.CurrentHealth == base.MaxHealth ? 40 : 30)) AiChoice = "S";
            else AiChoice = "A";
            return AiChoice;

        }
    }
}
