﻿using System;

namespace RPG
{
    class Roshan : Characters
    {
        public Roshan()
        {
            base.AiSpell = 0;
            base.AiAttack = 80;
            base.AiDefend = 100;
            base.CurrentHealth = 1000;
            base.MaxHealth = 1000;
            base.CurrentMagic = 0;
            base.MaxMagic = 0;
            base.Strength = 170;
            base.Defense = 80;
            base.Agility = 15;
            base.Intelligence = 20;
            base.Experience = OutputHelper.rand.Next(50, 70);
            base.Gold = OutputHelper.rand.Next(100, 130);
            base.Identifier = "Roshan";
            base.isAlive = true;
            base.isNamed = true;
            base.AttackDamage = Strength;
        }

    
        public override string AI()
        {
             
            string AiChoice;
            byte AiChoiceMaker = ((byte)(OutputHelper.rand.Next(1, 100)));
            if (AiChoiceMaker < base.AiAttack) AiChoice = "A";
            else if (AiChoiceMaker >= base.AiAttack && AiChoiceMaker <= base.AiDefend) AiChoice = "D";
            else if (AiChoiceMaker > base.AiSpell && AiChoiceMaker < base.AiDefend) AiChoice = "A";
            else AiChoice = "A";
            return AiChoice;

        }
    }
}
