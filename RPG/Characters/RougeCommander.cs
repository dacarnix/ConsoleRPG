﻿using System;

namespace RPG
{
    class RougeCommander:Characters
    {
        public RougeCommander()
        {
             
            base.AiSpell = 0;
            base.AiAttack = 80;
            base.AiDefend = 100;
            base.CurrentHealth = 300;
            base.MaxHealth = 300;
            base.CurrentMagic = 0;
            base.MaxMagic = 0;
            base.Strength = 150;
            base.Defense = 30;
            base.Agility = 25;
            base.Experience = OutputHelper.rand.Next(12, 30);
            base.Gold = OutputHelper.rand.Next(12, 19);
            base.Identifier = "Rogue Commander";
            base.isAlive = true;
            base.AttackDamage = Strength;
        }
        public override string AI()
        {
             
            string AiChoice;
            byte AiChoiceMaker = ((byte)(OutputHelper.rand.Next(1, 100)));
            if (AiChoiceMaker < base.AiAttack) AiChoice = "A";
            else if (AiChoiceMaker >= base.AiAttack && AiChoiceMaker <= base.AiDefend) AiChoice = "D";
            else if (AiChoiceMaker > base.AiSpell && AiChoiceMaker < base.AiDefend) AiChoice = "A";
            else AiChoice = "A";
            return AiChoice;

        }
    }
}
