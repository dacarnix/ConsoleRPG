﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace RPG
{
    class data
    {
        public data()
        {
        }

        public const string folderName = "Andrews_RPG";
        public const string saveFileName = "gamesave.xml";


        // Something 
        static bool value = false;
        public static void Save(Hero hero)
        {
            try
            {
                if (hero.secretVillage == true) hero.savePoint = "secret";
                else hero.savePoint = "village";
                string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                var basePath = Path.Combine(path, folderName + Path.DirectorySeparatorChar);
                if (!Directory.Exists(basePath)) Directory.CreateDirectory(basePath);

                var fullPath = Path.Combine(basePath, saveFileName);
                if (File.Exists(fullPath)) File.Delete(fullPath);

                FileStream stream = File.Open(fullPath, FileMode.OpenOrCreate);
                XmlSerializer serialize = new XmlSerializer(typeof(Hero));
                serialize.Serialize(stream, hero);
                stream.Close();
                Console.WriteLine("Saving successful!");
            }
            catch { Console.WriteLine("Error"); }
            BattleHelper.Anykey();
        }



        public static Hero Load(Hero hero)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                var basePath = Path.Combine(path, folderName + Path.DirectorySeparatorChar);
                var fullPath = Path.Combine(basePath, saveFileName);
                if (!Directory.Exists(basePath)) Directory.CreateDirectory(basePath);
                else if (File.Exists(fullPath)) // error here cause if no XML then it wont load
                {
                    FileStream stream = File.Open(fullPath, FileMode.OpenOrCreate, FileAccess.Read);
                    XmlSerializer serialize = new XmlSerializer(typeof(Hero));
                    hero = (Hero)serialize.Deserialize(stream);
                    stream.Close();

                }
                else
                {
                    Save(hero);
                    hero = Load(hero);

                }
                Console.WriteLine("Loading successful!");
            }
            catch { Console.WriteLine("error"); }
            BattleHelper.Anykey();

            if (hero.savePoint == "secret")
            {
                NewVillageMain vm = new NewVillageMain(hero);
            }
            else
            {
                MainGame main = new MainGame(value, hero);
            }
            return hero;
        }
    }
}
