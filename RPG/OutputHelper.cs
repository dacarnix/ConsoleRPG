﻿using System;

namespace RPG
{
    public static class OutputHelper
    {
        const int defaultColumnWidth = 25;

        public static Random rand = new Random();

        public static void columnedOutput(string first, string second, int columnWidth = defaultColumnWidth)
        {
            var paddedFirst = first.PadRight(columnWidth, ' ');
            Console.WriteLine(paddedFirst + second);
        }

        public static void columnedOutput(string first, string second, string third, int columnWidth = defaultColumnWidth, int secondColumnWidth = 0)
        {
            if (secondColumnWidth == 0)
            {
                secondColumnWidth = columnWidth;
            }
            
            if (third == null)
            {
                columnedOutput(first, second, columnWidth: columnWidth);
                return;
            }
            
            var paddedFirst = first.PadRight(columnWidth, ' ');
            var paddedSecond = second.PadRight(secondColumnWidth, ' ');
            Console.WriteLine(paddedFirst + paddedSecond + third);
        }
    }
}
