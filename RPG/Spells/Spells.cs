﻿namespace RPG
{

    class Spells
    {
       public int power;
       public int magicCost;
       public bool Lightning, Icebolt, Fireball;
       public bool MultipleHits, isOnSelf, SingleTarget;
       public string Identifier;
       public Spells()
        {
            MultipleHits = false;
            isOnSelf = false;
            SingleTarget = false;
            Lightning = false;
            Icebolt = false;
            Fireball = false;
        }
       public virtual int SpellCasterPower(Characters caster)
       {
           return power;
       }
    }
}
