﻿using System;

namespace RPG
{
    class FireBall:Spells
    {

        public FireBall(Characters attacker)
        {
             
            base.MultipleHits = true;
            base.power = OutputHelper.rand.Next((((attacker.Intelligence + attacker.IntelBuff) * 7) + 20), (((attacker.Intelligence + attacker.IntelBuff) * 10) + 40));;
            base.magicCost = 50;
            base.Identifier="Fireball";
        }

        public override int SpellCasterPower(Characters caster)
        {
            Console.Write("{0} casts a fireball ", caster.Identifier);
            caster.CurrentMagic -= magicCost;
            if (caster.CurrentMagic < 0) 
            {
                Console.WriteLine("however {0} did not have enough mana to use this!", caster.Identifier);
                caster.CurrentMagic += magicCost;
                power = 0;
            }
            else if (caster.CurrentMagic >= 0) Console.WriteLine("and hits for {0}hp!", power);
            return power;
        }
    }
}
