﻿using System;

namespace RPG
{
    class IceBall:Spells
    {
        public IceBall(Characters attacker)
        {
             
            base.Identifier = "IceBall";
            base.power = OutputHelper.rand.Next((((attacker.Intelligence + attacker.IntelBuff) * 8) + 10), (((attacker.Intelligence + attacker.IntelBuff)* 9) + 30));
            base.magicCost = 40;
            base.SingleTarget = true;
        }
        public override int SpellCasterPower(Characters caster)
        {
            Console.Write("{0} casts an Iceball ", caster.Identifier);
            caster.CurrentMagic -= magicCost;
            if (caster.CurrentMagic < 0)
            {
                Console.WriteLine("however {0} was unable to perform this spell!");
                caster.CurrentMagic += magicCost;
                power = 0;
            }
            else if (caster.CurrentMagic >= 0) Console.WriteLine("and hits {0}hp!", power);
            return power;
        }
    }
}
