﻿using System;

namespace RPG
{
    class Heal:Spells
    {
        public Heal(Characters attacker)
        {
            var totalIntel = (attacker.Intelligence + attacker.IntelBuff);
            base.isOnSelf = true;
            base.power = 120 + OutputHelper.rand.Next(totalIntel * 7, totalIntel * 16);
            base.magicCost = 30;
            base.Identifier = "Heal";
        }
        public override int SpellCasterPower(Characters caster)
        {
            Console.Write("{0} uses heal ", caster.Identifier);
            caster.CurrentMagic -= magicCost;
            if (caster.CurrentMagic < 0)
            {
                caster.CurrentMagic += magicCost;
                Console.WriteLine("but {0} does not have enough mana to use that spell!", caster.Identifier);
                power = 0;
            }
            else if (caster.CurrentMagic >= 0) Console.WriteLine("for {0}hp!", power);

            return power;
        }
    }
}
