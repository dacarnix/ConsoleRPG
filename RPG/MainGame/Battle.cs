﻿using System;
using System.Collections.Generic;

namespace RPG
{
    class Battle
    {
        string userspellchoice;
        string userchoice;
        string monsterchoice;
        string monsterspellchoice;
        int targetmonster;
        bool amonsterleft;

        public Battle(Hero hero, List<Characters> monsters)
        {
            BattleLoop(hero, monsters);
        }



        public void BattleLoop(Hero hero, List<Characters> monsters)
        {
            if (hero.CurrentHealth > hero.MaxHealth) hero.CurrentHealth = hero.MaxHealth;
            do
            {
                Console.Clear();
                Console.WriteLine("{0} is facing:", hero.Identifier);
                foreach (Characters monster in monsters)
                {
                    if (monster.Identifier == "Error Macro") Console.ForegroundColor = ConsoleColor.Red;
                    if (monster.isAlive)
                    {
                        Console.WriteLine("{0}", monster.Identifier);
                    }
                }
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine();
                
                hero.isAlive = BattleHelper.CheckHealth(hero.CurrentHealth);
                BattleHelper.PrintBattleHeader();
                BattleHelper.PrintBattleStatusBreaker();
                BattleHelper.PrintStatus(hero);
                foreach (Characters monster in monsters)
                {
                    if (monster.isAlive) BattleHelper.PrintStatus(monster);         
                }
                userchoice = BattleHelper.PrintChoice(hero);

                Console.WriteLine();
                BattleHelper.CheckDefence(userchoice, hero);
                if (userchoice == "s" || userchoice == "S") userspellchoice = BattleHelper.PrintSpells(hero);
                else if (userchoice == "f" || userchoice == "F")
                {
                    Console.WriteLine("You succesfully run away from the battle");
                    hero.fled = true;
                    BattleHelper.Anykey();
                    break ;
                }
                
                targetmonster = BattleHelper.ChooseTarget(monsters, userspellchoice);
                BattleHelper.ProcessChoice(userchoice, hero, monsters[targetmonster], userspellchoice);

                foreach (Characters monster in monsters)
                {
                    monster.isAlive = BattleHelper.CheckHealth(monster.CurrentHealth);
                    if (monster.isAlive == true)
                    {
                        monsterchoice = monster.AI();
                        BattleHelper.CheckDefence(monsterchoice, monster);
                        if (monsterchoice == "S" || monsterchoice == "s")
                        {
                            monsterspellchoice = monster.SpellAI();
                        }
                        
                        BattleHelper.ProcessChoice(monsterchoice, monster, hero, monsterspellchoice);
                       
                    }
                }
                amonsterleft = BattleHelper.CheckMonsters(monsters,hero);
                hero.isAlive = BattleHelper.CheckHealth(hero.CurrentHealth);
                Console.WriteLine();
                BattleHelper.Anykey();
            }
            while (hero.isAlive == true && amonsterleft == true);

            if (hero.isAlive)
            {
                int itemSelect = OutputHelper.rand.Next(100);
                if (itemSelect <= 19 + (hero.Level / 2)) BattleHelper.RandomItem(hero);
            }
        }
    }
}
    


