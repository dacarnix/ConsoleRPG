﻿using RPG.Items;
using System;
using System.Collections.Generic;

namespace RPG
{

    class BattleHelper
    {
        static int damage;

        #region CheckHealth
        public static bool CheckHealth(int health)
        {
            bool alive;
            if (health > 0)
            {
                alive = true;
            }
            else
            {
                alive = false;
            }
            return alive;
        }
        #endregion

        #region DealDamage
        public static int DealDamage(Characters attacker, Characters defender)
        {
            int max;
            int min;
            max = attacker.AttackDamage - defender.Defense;
            if (max <= 5) max = 5;
            min = (int)(attacker.AttackDamage * .8) - defender.Defense;
            if (min <= 1) min = 1;
            damage = OutputHelper.rand.Next(min, max);
            if (attacker.increaseAttack) damage = (int)(damage * 1.5);
            if (defender.defending) damage /= 2;
            if (defender.spellDefending)
            {
                damage /= 2;
                defender.spellDefending = false;
            }

            if (damage <= 1) damage = 1;

            // Old logic -  if (crit >= (1000 - ((attacker.Agility + attacker.AgilityBuff) * 12)))
            int crit = OutputHelper.rand.Next(100);
            var critChance = 100 - (Math.Sqrt((attacker.Agility + attacker.AgilityBuff) * 15) + 2);
            if (crit >= critChance)
            {
                damage = (int)(damage * 1.5);
                Console.WriteLine("{0} hits a critical strike!", attacker.Identifier, damage);
            }

            return damage;
        }
        #endregion

        #region ProcessChoice
        public static void ProcessChoice(string choice, Characters attacker, Characters defender, string spellchoice)
        {
            string Potion;
            switch (choice)
            {
                case "A":
                case "a":
                    Console.WriteLine();
                    DealDamage(attacker, defender);
                    defender.CurrentHealth -= damage;
                    if (defender.isNamed && !attacker.isNamed)
                    {
                        Console.WriteLine("The {0} hits {1} for {2} damage{3}"
                        , attacker.Identifier, defender.Identifier, damage, (defender.CurrentHealth <= 0 ? " and defeats them" : ""));
                    }
                    else if (!defender.isNamed && attacker.isNamed)
                    {
                        Console.WriteLine("{0} hits the {1} for {2} damage{3}"
                        ,attacker.Identifier,defender.Identifier, damage, (defender.CurrentHealth <= 0 ? " and defeats them" : ""));
                    }
                    else
                    {
                        Console.WriteLine("{0} hits {1} for {2} damage{3}"
                       , attacker.Identifier, defender.Identifier, damage, (defender.CurrentHealth <= 0 ? " and defeats them" : ""));
                    }
                    break;
                case "D":
                case "d":
                    Console.WriteLine();
                    Console.WriteLine("{0} defends!", attacker.Identifier);
                    break;
                case "F":
                case "f":
                    Console.WriteLine();
                    Console.WriteLine("{0} flees!", attacker.Identifier);
                    attacker.fled = true;
                    attacker.isAlive = false;
                    break;
                case "S":
                case "s":
                    Console.WriteLine();
                    bool succesfullyCastedASpell = CastSpell(attacker, defender, spellchoice);
                    if (!succesfullyCastedASpell)
                    {
                        recoverBattleLoop(attacker, defender, spellchoice);
                    }
                    break;
                case "P":
                case "p":
                    bool validChoice = true;
                    bool useNone = false;
                    bool hasPotions = attacker.ManaPotions > 0 || attacker.HealthPotions > 0;
                    if (hasPotions)
                    {
                        Console.WriteLine("\nWhat potion would you like to use?\n");
                        if (attacker.HealthPotions > 0)
                        {
                            OutputHelper.columnedOutput("(H)ealth Potion", "Current Amount " + attacker.HealthPotions, columnWidth: 20);
                        }

                        if (attacker.ManaPotions > 0)
                        {
                            OutputHelper.columnedOutput("(M)ana Potion", "Current Amount " + attacker.ManaPotions, columnWidth: 20);
                        }

                        Console.WriteLine("(U)se none");

                        Potion = Console.ReadLine().ToLower();

                        if (Potion == "u")
                        {
                            useNone = true;
                         
                        } else
                        {
                            validChoice = PotionProcess(attacker, Potion);
                        }
                    }

                    if (!validChoice || !hasPotions || useNone)
                    {
                        if (!useNone)
                        {
                            Console.WriteLine(hasPotions ? "I'm sorry, that is an invalid potion option." : "You do not have potions to use.");
                        }

                        recoverBattleLoop(attacker, defender, spellchoice);
                    }
                    break;
                default:
                    Console.WriteLine("I'm sorry, I didn't recognize that option.");
                    choice = PrintChoice(attacker);
                    if (choice == "s" || choice == "S") spellchoice = BattleHelper.PrintSpells(attacker);
                    ProcessChoice(choice, attacker, defender, spellchoice);
                break;
            }
        }

        private static void recoverBattleLoop(Characters attacker, Characters defender, string spellchoice)
        {
            BattleHelper.Anykey(clear: false);
            var choice = PrintChoice(attacker);
            if (choice == "s" || choice == "S") spellchoice = BattleHelper.PrintSpells(attacker);
            ProcessChoice(choice, attacker, defender, spellchoice);
        }

        #endregion

        #region PrintStatus
        public static void PrintBattleHeader()
        {
            OutputHelper.columnedOutput("", "HP/MaxHP", "MP/MaxMP", columnWidth: 15);
        }
        
        public static void PrintStatus(Characters hero)
        {
            if (hero.Identifier == "Error Macro") Console.ForegroundColor = ConsoleColor.Red;
            OutputHelper.columnedOutput(hero.Identifier+":", hero.CurrentHealth + "/" + hero.MaxHealth + "hp", (hero.MaxMagic == 0 ? "" : (hero.CurrentMagic + "/" + hero.MaxMagic + "mp")), columnWidth: 15);
            PrintBattleStatusBreaker();
            Console.ForegroundColor = ConsoleColor.White;
        }
        
        public static void PrintBattleStatusBreaker()
        {
            Console.WriteLine("*".PadRight(45, '*'));
        }
        
        #endregion

        #region PrintChoice
        /// <summary>
        /// This method prints our choices and gets the choice.
        /// </summary>
        /// <returns>returns the string of the hero's choice</returns>
        public static string PrintChoice(Characters hero)
        {
            string choice;
            Console.WriteLine();
            if (hero.isRoaming == false)
            {
                Console.Write(@"________________________
Please choose an action:
(A)ttack
(D)efend
(P)otion
(S)pell
(F)lee
________________________");
                Console.WriteLine();
                choice = Console.ReadLine().ToLower();
            }
            else
            {
                Console.Write(@"________________________
Please choose an action:
(A)ttack
(D)efend
(P)otion
(S)pell
________________________");
                Console.WriteLine();
                choice = Console.ReadLine().ToLower();
            }
            return choice;
        }
        #endregion

        #region CheckDefence

        public static void CheckDefence(string choice, Characters attacker)
        {
            if (attacker.defending) attacker.increaseAttack = true;
            else attacker.increaseAttack = false;
            
            if (choice == "D" || choice == "d") attacker.defending = true;
            else attacker.defending = false;
        }
        #endregion

        #region CastSpell
        public static bool CastSpell(Characters attacker, Characters defender, string spellchoice)
        {
            Spells spell;
            spell = ProcessSpellChoice(spellchoice, attacker);
            if (spell == null)
            {
                // Terminated spell choice
                return false;
            }
            else if (spell.magicCost > attacker.CurrentMagic)
            {
                Console.WriteLine("You have insufficient mana to cast this spell");
                return false;
            }

            int spellpower = spell.SpellCasterPower(attacker);
            if (spell.isOnSelf == true)
            {
                attacker.CurrentHealth += spellpower;
                if (attacker.CurrentHealth > attacker.MaxHealth) attacker.CurrentHealth = attacker.MaxHealth;
            }
            else if (spell.MultipleHits == true) defender.CurrentHealth -= spellpower;
            else if (spell.SingleTarget == true) defender.CurrentHealth -= spellpower;

            return true;
        }
        #endregion

        #region PrintSpells
        public static string PrintSpells(Characters attacker)
        {
            Console.WriteLine();
            string spellchoice;
            Console.Write(@"
Please choose a spell:
***********************
Current mana is {0}
(H)eal       Cost: 30MP
(I)cebolt    Cost: 40MP
(F)ireball   Cost: 50MP

(G)o back
***********************", attacker.CurrentMagic);
            Console.WriteLine();
            spellchoice = Console.ReadLine().ToLower();
            return spellchoice;
        }
        #endregion

        #region ProcessSpellChoice
        public static Spells ProcessSpellChoice(string spellchoice, Characters attacker)
        {
            Spells spell;
            switch (spellchoice)
            {
                case "H":
                case "h":
                    Heal heal = new Heal(attacker);
                    attacker.spellDefending = true;
                    return heal;
                case "F":
                case "f":
                    FireBall fireball = new FireBall(attacker);
                    return fireball;
                case "I":
                case "i":
                    IceBall icebolt = new IceBall(attacker);
                    return icebolt;
                case "G":
                case "g":
                    return null;
                default:
                    Console.WriteLine();
                    Console.WriteLine("I'm sorry that wasn't a valid choice");
                    spellchoice = PrintSpells(attacker);
                    spell = ProcessSpellChoice(spellchoice, attacker);
                    break;
            }
            return spell;
        }
        #endregion

        #region CheckMonsters
        public static bool CheckMonsters(List<Characters> Monsters, Hero hero)
        {
            foreach (Characters monster in Monsters)
            {
                if (monster.isAlive) return true;
            }

            if (Monsters[0].Identifier == "Rogue Commander" && hero.roamingKey == false)
            {
                hero.roamingKey = true;
                Console.WriteLine(@"
Congratulations!
You have defeated the Rogue Commander
You are now able to go Roaming!");
            }
            else if (Monsters[0].Identifier == "Error Macro" && !hero.Weapons.Contains("(E)rror Sword, "))
            {
                Console.WriteLine(@"
Congratulations!
You have defeated Error Macro
You have collected his sword!");
                // Do not re-add it many times.
                if (!hero.Weapons.Contains("(E)rror Sword, "))
                {
                    hero.Weapons.Add("(E)rror Sword, ");
                }
            }
            else if (Monsters[0].Identifier == "Roshan" && !hero.Boss1Defeated)
            {
                Console.WriteLine(@"
Congratulations!
You have defeated Roshan!
You may now fight the next boss");
                hero.Boss1Defeated = true;
            }
            else if (Monsters[0].Identifier == "Kongor" && !hero.Boss2Defeated)
            {
                Console.WriteLine(@"
Congratulations!
You have defeated Kongor!
You may now fight the next boss..");
                hero.Boss2Defeated = true;
            }
            else if (Monsters[0].Identifier == "Reptile" && !hero.Boss3Defeated)
            {
                Console.WriteLine(@"
Congratulations!
You have defeated Reptile!
You may now fight the last boss..");
                hero.Boss3Defeated = true;
            }
            else if (Monsters[0].Identifier == "Liu King")
            {
                Console.WriteLine(@"
Congratulations!
You have defeated Liu King!
You collected his Fire Sword!
You have completed the game :O, until next update ;)");
                // "(F)ire Sword, "
                if (!hero.Weapons.Contains("(F)ire Sword, "))
                {
                    hero.Weapons.Add("(F)ire Sword, ");
                }
                
                hero.GameWon = true;
            }

            return false;
        }
        #endregion

        #region ChooseTarget
        public static int ChooseTarget(List<Characters> Monster, string spell)
        {
            int i = 0;
            foreach (Characters monster in Monster)
            {
                i++;
            }
            if (i == 1 || spell == "H" || spell == "h")
            {
                i = 0;
                return i;
            }
            string choice;
            int x = 0;

            int aliveCount = 0;
            foreach (Characters monster in Monster)
            {
                if (monster.isAlive)
                {
                    aliveCount++;
                }
            }

            var onlyOneLeftAlive = aliveCount == 1;

            if (!onlyOneLeftAlive)
            {
                Console.WriteLine("Please choose the monster to attack");
            }

            foreach (Characters monster in Monster)
            {
                x++;
                if (monster.isAlive)
                {
                    if (onlyOneLeftAlive) return x - 1;
                    else
                    {
                        Console.WriteLine("{0}: {1}", x, monster.Identifier);
                    }
                }
            }
            Console.WriteLine();
            choice = Console.ReadLine();
            //below is an example of exception
            //handling.
            try//try this stuff
            {
                x = int.Parse(choice);
            }
            catch (Exception)//if problem try this
            {
                Console.WriteLine("Invalid choice");
                x = ChooseTarget(Monster, spell);
                x++;
            }
            finally//finally when it works do this
            {
                x -= 1;
            }
            return x;

        }
        #endregion
        public static void HeroDeathVoid(Hero hero)
        {
            if (hero.isAlive == false)
            {
                if (hero.Gold >= 10)
                {
                    hero.Gold -= 10;
                    hero.CurrentHealth = hero.MaxHealth;
                    hero.CurrentMagic = hero.MaxMagic;
                    Console.WriteLine("You have been healed for 10 gold in the nearest Inn");
                    BattleHelper.Anykey();
                    hero.isAlive = true;
                }
                else
                {
                    Console.WriteLine(@"Since you could not afford to be healed, you have been left to die. :(

The game will automatically try to re-load from your last save");
                    BattleHelper.Anykey();
                    data.Load(hero);

                }
            }
        }
        public static Hero Leveling(Hero hero)
        {
            if (hero.SkillPoints == 0)
            {
                Console.WriteLine("Sorry you dont have any skill points to use");
                BattleHelper.Anykey();
                return hero;
            }
            else if (hero.SkillPoints >= 1)
            {
                Console.Clear();
                Console.WriteLine(string.Format(@"
You have {0} skill points to spend!

Please select a skill to upgrade", hero.SkillPoints));

                OutputHelper.columnedOutput("(A)gility = " + hero.Agility, "(Adds 4 defense and increases critical strike chance per level)");
                OutputHelper.columnedOutput("(S)trength = " + hero.Strength, "(Adds 4 max health and 6 attack damage per level)");
                OutputHelper.columnedOutput("(I)ntelligence = " + hero.Intelligence, "(Adds 10 max mana and improves magic damage per level)");
                
                Console.WriteLine("\n(L)eave without spending your point(s)");
            }
            while (true)
            {
                string decision = Console.ReadLine();

                if (decision == "A" || decision == "a")
                {
                    hero.Agility += 1; // Needa add UNDO
                    hero.SkillPoints -= 1;
                    Console.WriteLine("\nAgility is now {0}", hero.Agility);
                    break;
                }
                else if (decision == "S" || decision == "s")
                {
                    hero.Strength += 1;
                    hero.SkillPoints -= 1;
                    Console.WriteLine("\nStrength is now {0}", hero.Strength);
                    break;
                }
                else if (decision == "I" || decision == "i")
                {
                    hero.Intelligence += 1;
                    hero.SkillPoints -= 1;
                    Console.WriteLine("\nIntelligence is now {0}", hero.Intelligence);
                    break;
                }
                else if (decision == "L" || decision == "l")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("I didn't understand that decision.\n");
                }
            }

            BattleHelper.Anykey();
            RefreshStats(hero);
            return hero;
        }
        public static Hero RefreshStats(Hero hero)
        {
            hero.AttackDamage = ((hero.Strength + hero.StrengthBuff) * 6) + hero.WeaponAttack;
            hero.MaxHealth = ((hero.Strength + hero.StrengthBuff) * 4) + 130 + (hero.Level * 10) + hero.HealthBuff;
            
            hero.Defense = ((hero.Agility + hero.AgilityBuff) * 4) + hero.HelmetDefense + hero.ChestDefense + hero.LegDefense;
            hero.MaxMagic = (hero.Intelligence + hero.IntelBuff) * 10;

            // Incase they removed an item with a health/magic buff
            hero.CurrentHealth = Math.Min(hero.CurrentHealth, hero.MaxHealth);
            hero.CurrentMagic = Math.Min(hero.CurrentMagic, hero.MaxMagic);

            return hero;
        }
        #region AnyKey
        public static void Anykey(bool clear = true)
        {
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();

            if (clear)
            {
                Console.Clear();
            }
        }
        #endregion
        #region Potions
        public static bool PotionProcess(Characters attacker, string Potion)
        {
            if (Potion == "m" || Potion == "M")
            {
                if (attacker.ManaPotions >= 1)
                {
                    attacker.spellDefending = true;
                    attacker.ManaPotions -= 1;
                    attacker.CurrentMagic += 100;
                    if (attacker.CurrentMagic > attacker.MaxMagic) attacker.CurrentMagic = attacker.MaxMagic;
                    Console.WriteLine("{0} used a potion to heal 100 mana!", attacker.Identifier);
                    return true;
                }
            }
            else if (Potion == "h" || Potion == "H")
            {
                if (attacker.HealthPotions >= 1)
                {
                    attacker.spellDefending = true;
                    attacker.HealthPotions -= 1;
                    attacker.CurrentHealth += 200;
                    if (attacker.CurrentHealth > attacker.MaxHealth) attacker.CurrentHealth = attacker.MaxHealth;
                    Console.WriteLine("{0} used a potion to heal 200 hp!", attacker.Identifier);
                    return true;
                }
            }
            
            return false;
        }
        #endregion
        public static void expCheck(Hero hero)
        {
            if (hero.Experience >= (hero.Level * 6 + 8))
            {
                int experienceOverFlow = (hero.Experience - (hero.Level * 6 + 8));
                hero.Level += 1;
                hero.SkillPoints += 1;
                hero.Experience = experienceOverFlow;
                BattleHelper.RefreshStats(hero);
                Console.WriteLine(@"
Congratz! You have leveled up to level {0}!
Go to the main menu to upgrade
the skill point you just earned!
", hero.Level);
            }
            if (hero.Experience >= (hero.Level * 6 + 8)) expCheck(hero);
        }
        public static void RandomItem(Hero hero)
        {
            List<BaseItem> itemList = new List<BaseItem>();
            int itemPick;
            Console.Clear();
            Console.WriteLine("Some loot was dropped behind with your recent victory!\n");
             
            if (hero.Level <= 4) 
            {
                itemList.Add(new DirtyDagger());
                itemList.Add(new DirtyCap());
                itemList.Add(new DirtyLeggings());
                itemList.Add(new Hatchet());
                try { ItemLoot(itemList[OutputHelper.rand.Next(0, 4)], hero); }
                catch { ItemLoot(itemList[0], hero); }
                itemList.Clear();
            }
            else if (hero.Level <= 7)
            {
                itemPick = OutputHelper.rand.Next(1, 100);
                if (itemPick <= 30) itemList.Add(new DirtyPlate());
                else if (itemPick > 30 && itemPick <= 50) itemList.Add(new BasicStaff());
                else if (itemPick > 50 && itemPick <= 65) itemList.Add(new Scimitar());
                else if (itemPick > 65 && itemPick <= 75) itemList.Add(new LeatherHelmet());
                else if (itemPick > 75 && itemPick <= 85) itemList.Add(new LeatherPlate());
                else itemList.Add(new Fortunate());
                ItemLoot(itemList[0], hero);
                itemList.Clear();
            }
            else
            {
                itemPick = OutputHelper.rand.Next(1, 100);
                if (itemPick <= 30) itemList.Add(new IronHelmet());
                else if (itemPick > 30 && itemPick <= 50) itemList.Add(new Scimitar());
                else if (itemPick > 50 && itemPick <= 65) itemList.Add(new IronLeggings());
                else if (itemPick > 65 && itemPick <= 75) itemList.Add(new DiamondHelmet());
                else if (itemPick > 75 && itemPick <= 85) itemList.Add(new Katana());
                else itemList.Add(new Fortunate());
                ItemLoot(itemList[0], hero);
                itemList.Clear();
            }
        }
        #region ItemDropLoot
        public static void ItemLoot(BaseItem item, Hero hero)
        {
            if (hero.Helmets.Contains(item.Identifier + ", "))
            {
                Console.WriteLine("{0} won't fit! You already have this item", item.Identifier);
            }
            else if (hero.Plates.Contains(item.Identifier + ", "))
            {
                Console.WriteLine("{0} won't fit! You already have this item", item.Identifier);
            }
            else if (hero.Leggings.Contains(item.Identifier + ", "))
            {
                Console.WriteLine("{0} won't fit! You already have this item", item.Identifier);
            }
            else if (hero.Weapons.Contains(item.Identifier + ", "))
            {
                Console.WriteLine("{0} won't fit! You already have this item", item.Identifier);
            }
            else
            {
                if (item.WearingArea == "Head") hero.Helmets.Add(item.Identifier + ", ");
                else if (item.WearingArea == "Chest") hero.Plates.Add(item.Identifier + ", ");
                else if (item.WearingArea == "Legs") hero.Leggings.Add(item.Identifier + ", ");
                else if (item.WearingArea == "Arm") hero.Weapons.Add(item.Identifier + ", ");
                Console.WriteLine("You have found:");
                item.Identifier.PrintItemWithBuffs(sanitizeName: true);
            }
            
            Console.WriteLine();
            Anykey();
        }
        #endregion
    }
    
    
       

        

}
