﻿using System;
using System.Collections.Generic;

namespace RPG
{
    class MainGame
    {
        List<Characters> Monster;
        Hero myhero;
        Battle battle;
        string answer;
        bool Load;
                    string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        public MainGame(bool load, Hero hero)
        {
            Load = load;
            myhero = new Hero();
            myhero = hero;
            BasicGameLoop();
        }

      void BasicGameLoop()
        {
           do
            {
                myhero.isRoaming = false;
                myhero.secretVillage = false;
                myhero.savePoint = "village";
             if (Load == true)
             {
                 myhero = data.Load(myhero);
                 Load = false;
             }
             BattleHelper.HeroDeathVoid(myhero);
             Console.Clear();
                Console.WriteLine("Welcome Home, {0}! :)\n\nYou have {1}/{2}hp, {3}/{4}mp and {5} gold.\n", myhero.Identifier, myhero.CurrentHealth, myhero.MaxHealth, myhero.CurrentMagic, myhero.MaxMagic, myhero.Gold);
                Console.Write(@"What would you like to do?
_____________________________
(F)ight 
(R)oam
(S)tore
(I)nn (rest)
Eq(U)ip Items
L(E)vel Up Stats{0}
s(A)ve
(V)iew stats and items
(Q)uit
_____________________________", myhero.SkillPoints > 0 ? " - " + myhero.SkillPoints + " point to use" : "");

                Console.WriteLine();
                answer = Console.ReadLine().ToLower();
                Console.WriteLine();
                switch (answer)
                {
                    case "gimme all":
                        myhero.Gold += 10000;
                        myhero.Strength += 30;
                        myhero.Intelligence += 30;
                        myhero.Agility += 30;
                        BattleHelper.RefreshStats(myhero);
                        BattleHelper.Anykey();
                        break;
                    case "s":
                        Shop store = new Shop(myhero);
                        break;
                    case "i":
                        Inn.Sleep(myhero);
                        break;
                    case "v":
                        View.PrintStats(myhero);
                        break;
                    case "e":
                        myhero = BattleHelper.Leveling(myhero);
                        break;
                    case "a":
                        data.Save(myhero);
                        break;
                    case "u":
                        EquipItems.Equip(myhero);
                        break;
                    case "r":
                        RoamingMain main = new RoamingMain(myhero);
                        break;
                    case "f":
                        Monster = new List<Characters>();
                        Console.Clear();
                        string choice = "";
                        do
                        {
                            Console.Write(@"Which monster do you want to fight?

m(O)nster
(S)lime
(R)ogue
(M)age
Rogue (C)ommander - *Boss*

(G)o back
_________________________");
                            Console.WriteLine();
                            choice = Console.ReadLine().ToLower();
                            if (choice == "S" || choice == "s") Monster.Add(new Slime());
                            else if (choice == "O" || choice == "o") Monster.Add(new Monster());
                            else if (choice == "R" || choice == "r") Monster.Add(new Rouge());
                            else if (choice == "C" || choice == "c") Monster.Add(new RougeCommander());
                            else if (choice == "M" || choice == "m") Monster.Add(new Mage());
                            else if (choice == "g") break;
                            else
                            {
                                Console.WriteLine("I don't understand that input.");
                            }
                            //Console.WriteLine("Would you like to fight more monsters?");
                            //Console.WriteLine();
                            //done = Console.ReadLine();
                        }
                        while (Monster.Count == 0);

                        if (choice == "g") break;

                        battle = new Battle(myhero, Monster);
                        if (myhero.CurrentHealth <= 0)
                        {
                            Console.WriteLine("You have been severly injured in battle by {0}", Monster[0].Identifier);
                            break;
                        }
                        else if (myhero.fled == false)
                        {
                            int gold = 0;
                            int experience = 0;
                            foreach (Characters monster in Monster)
                            {
                                if (monster.fled == false)
                                {
                                    experience += monster.Experience;
                                    gold += (monster.Gold + myhero.GoldBuff);
                                }
                            }
                            Console.WriteLine("{0} gets {1} gold and {2} experience for defeating the {3}"
                                , myhero.Identifier, gold, experience, Monster[0].Identifier);
                            myhero.Experience += experience;
                            myhero.Gold += gold;
                            BattleHelper.expCheck(myhero);
                            Monster.Clear();
                            BattleHelper.Anykey();
                        }
                        else myhero.fled = false;
                        break;
                    case "Q":
                    case "q":
                        string saveChoice;
                        Console.WriteLine("Would you like to save before you leave? (Y/N)");
                        saveChoice = Console.ReadLine().ToLower();
                        if (saveChoice == "y" || saveChoice == "yes") data.Save(myhero);
                        Console.WriteLine("Goodbye {0}", myhero.Identifier);
                        answer = "Q";
                        Environment.Exit(0);
                        break;
                }
            }
            while (answer != "Q" && answer != "q");
        }
            




    }
   

}
