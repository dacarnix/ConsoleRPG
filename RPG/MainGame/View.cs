﻿using RPG.Items;
using System;

namespace RPG
{
    public class View
    {
        public static void PrintStats(Hero hero)
        {
            
            Console.Clear();
            OutputHelper.columnedOutput("Name: " + hero.Identifier, "Hitpoints: " + hero.CurrentHealth + '/' + hero.MaxHealth, "Mana: " + hero.CurrentMagic + '/' + hero.MaxMagic);
            OutputHelper.columnedOutput("Gold: " + hero.Gold, "Health Potions: " + hero.HealthPotions, "Mana Potions: " + hero.ManaPotions);
            Console.WriteLine();
            OutputHelper.columnedOutput("Attack: " + hero.AttackDamage, "Defense: " + hero.Defense);

            OutputHelper.columnedOutput(
                "Total Strength: " + (hero.Strength + hero.StrengthBuff),
                "Total Agility: " + (hero.Agility + hero.AgilityBuff),
                "Total Intelligence: " + (hero.Intelligence + hero.IntelBuff));
            Console.WriteLine();
            OutputHelper.columnedOutput("Level: " + hero.Level, "Experience needed for next level: " + ((hero.Level * 6 + 8) - hero.Experience));

            Console.WriteLine(@"

Helmets:");
            foreach (string item in hero.Helmets)
            {
                var equipt = item.SanitizeItemName() == hero.Helmet;
                item.PrintItemWithBuffs(sanitizeName: true, isEquipt: equipt);
            }
            if (hero.Helmets.Count == 0) Console.WriteLine("None yet");
            Console.WriteLine(@"
Plates:");
            foreach (string item in hero.Plates)
            {
                var equipt = item.SanitizeItemName() == hero.Plate;
                item.PrintItemWithBuffs(sanitizeName: true, isEquipt: equipt);
            }
            if (hero.Plates.Count == 0) Console.WriteLine("None yet");
            Console.WriteLine(@"
Leggings:");
            foreach (string item in hero.Leggings)
            {
                var equipt = item.SanitizeItemName() == hero.Legging;
                item.PrintItemWithBuffs(sanitizeName: true, isEquipt: equipt);
            }
            if (hero.Leggings.Count == 0) Console.WriteLine("None yet");
            Console.WriteLine(@"
Weapons:");
            foreach (string item in hero.Weapons)
            {
                var equipt = item.SanitizeItemName() == hero.Weapon;
                item.PrintItemWithBuffs(sanitizeName: true, isEquipt: equipt);
            }
            if (hero.Weapons.Count == 0) Console.WriteLine("None yet");
            Console.WriteLine();
            BattleHelper.Anykey();
        }
    }

}
