﻿using System;

namespace RPG
{
    public class Inn
    {


            public static void Sleep(Hero hero)
            {
                Console.Clear();
                string answer = "";
                Console.WriteLine("Hello and welcome to the Inn, it's 8 gold to stay.\n");

                Console.WriteLine("Your current gold is {0}.", hero.Gold);
                Console.WriteLine("Your current health is {0}, and mana {1}.", hero.CurrentHealth.ToString() + '/' + hero.MaxHealth.ToString(), hero.CurrentMagic.ToString() + '/' + hero.MaxMagic.ToString());
                do
                {
                    Console.WriteLine("\nWhat can I do for you?");
                    Console.Write(@"
(R)est
(L)eave
");
                    answer = Console.ReadLine();
                    Console.WriteLine();
                    switch (answer)
                    {
                        case "R":
                        case "r":
                            if (hero.Gold >= 8)
                            {
                                hero.Gold -= 8;
                                Console.WriteLine("You slept the whole night, your health and mana have been restored!");
                                hero.CurrentHealth = hero.MaxHealth;
                                hero.CurrentMagic = hero.MaxMagic;
                            }
                            else
                            {
                                Console.WriteLine("Hey you don't have enough gold!");
                            }
                            break;
                        case "L":
                        case "l":
                            Console.WriteLine("Be careful out there!");
                            break;
                        default:
                            Console.WriteLine("What? I don't understand that.");
                            break;
                    }

                }
                while ((answer.ToLower() != "l" && answer.ToLower() != "r"));

                BattleHelper.Anykey();
                
            }
        }

    }

