﻿namespace RPG
{
    public class BaseItem 
    {
        public int DefenceRating;
        public int AttackDamage;
        public int ManaRestore, HealthRestore; // to do, add enchantments XD HealthRegen,ManaRegen,
        public int Cost,sellValue;
        public int StrengthBuff, AgilityBuff, IntelligenceBuff,HealthBuff;
        public string WearingArea, Identifier;
        public BaseItem()
        {
            AttackDamage = 0;
            DefenceRating = 0;
            Cost = 0;
            ManaRestore = 0;
            HealthRestore = 0;
            
        }
 

    }

}
