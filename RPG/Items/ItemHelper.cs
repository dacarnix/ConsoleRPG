﻿using System;
using System.Collections.Generic;

namespace RPG.Items
{
    static class ItemHelper
    {
        // Really gross way of doing this, but the legacy code is even worse, this is the shortest way I could see
        // to be compatible with PrintItemWithBuffs and the old code, I hope I didn't miss anything in this list.
        static BaseItem[] allItems = {
            new DirtyCap(),
            new DirtyPlate(),
            new DirtyLeggings(),

            new LeatherHelmet(),
            new LeatherPlate(),
            new LeatherLeggings(),

            new IronHelmet(),
            new IronPlate(),
            new IronLeggings(),

            new DiamondHelmet(),
            new DiamondPlate(),
            new DiamondLeggings(),

            new PureAwesomeHelmet(),
            new SuperMegaAwesomePlate(),
            new SuperAwesomeLeggings(),

            new Fortunate(), // fortunate chest plate

            // All weapons below
            new DirtyDagger(),
            new Hatchet(),
            new BasicStaff(),
            new Scimitar(),
            new Katana(),
            new AwesomeSword(),
            new ManlyHammer(),
            new WizardStaff(),
            new SangeAndYasha(),
            new PwnHammer(),
            new ErrorSword(),
            new ImpoweringSword(),
            new Fire_Blade(),
        };

        static Dictionary<string, BaseItem> itemHash = new Dictionary<string, BaseItem>();


        static ItemHelper() {
            foreach (var item in allItems)
            {
                itemHash.Add(item.Identifier.SanitizeItemName(), item);
            }
        }
        
        private static string GetBuffString(BaseItem item)
        {
            if (item is Fortunate)
            {
                // Special case, the only item with a gold buff
                return "+3 extra gold on kill";
            }
            string buffs = "";
            
            if (item.StrengthBuff > 0)
            {
                buffs += $"+{item.StrengthBuff} STR, ";
            }
            
            if (item.AgilityBuff > 0)
            {
                buffs += $"+{item.AgilityBuff} AGI, ";
            }
            
            if (item.IntelligenceBuff > 0)
            {
                buffs += $"+{item.IntelligenceBuff} INT, ";
            }

            if (item.HealthBuff > 0)
            {
                buffs += $"+{item.HealthBuff} Max HP, ";
            }

            if (string.IsNullOrWhiteSpace(buffs))
            {
                return null;
            }
            
            return buffs.Trim().Remove(buffs.Length - 2);
        }
        
        public static void PrintItemWithBuffs(this string item, bool sanitizeName = false, bool isEquipt = false)
        {
            var sanitizedInput = item.SanitizeItemName();
            var name = sanitizeName ? sanitizedInput : item;
            if (isEquipt)
            {
                name += " (equipt)";
            }
            
            if (itemHash.ContainsKey(sanitizedInput))
            {
                var itemObject = itemHash[sanitizedInput];
                var buffString = GetBuffString(itemObject);

                if (itemObject is Weapons)
                {
                    OutputHelper.columnedOutput(name, "Damage: " + itemObject.AttackDamage, buffString, columnWidth: 33, secondColumnWidth: 20);
                }
                else
                {
                    OutputHelper.columnedOutput(name, "Defense: " + itemObject.DefenceRating, buffString, columnWidth: 33, secondColumnWidth: 20);
                }
            } 
            else
            {
                // Item is probably missing from the all items list above.
                Console.WriteLine(sanitizeName ? sanitizedInput : item);
            }
        }
        
        public static string SanitizeItemName(this string item)
        {
            var components = item.Replace("(", "")
               .Replace(")", "")
               .Replace(",", "")
               .Trim()
               .ToLower()
               .Split(' ');

            string result = "";
            for (int i = 0; i < components.Length; i++)
            {
                if (i > 0) result += ' ';
                result += components[i][0].ToString().ToUpper() + components[i].Substring(1);
            }

            return result;
        }
    }
}
