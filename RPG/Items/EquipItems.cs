﻿using RPG.Items;
using System;

namespace RPG
{
    class EquipItems
    {
        public EquipItems(Hero hero)
        {

        }
        public static void Equip(Hero hero)
        {
            string choice;
            int AmountCheck = 0;
            string itemChoice;
            Console.WriteLine(@"
What item would you like to equip?
(H)elmet
(C)hestplate
(L)eggings
(W)eapons
Ch(E)ck equipped items
(U)nequip all items

(D)one");
            choice = Console.ReadLine();
          
            switch (choice)
            {
                case "H":
                case "h":
                    Console.WriteLine(@"
You have ");
                    foreach (string s in hero.Helmets)
                    {
                        AmountCheck++;
                        var equipt = s.SanitizeItemName() == hero.Helmet;
                        s.PrintItemWithBuffs(sanitizeName: false, isEquipt: equipt);
                    }
                    if (AmountCheck >= 1)
                    {
                        Console.WriteLine("\nWhich helmet would you like to equip?");
                        itemChoice = Console.ReadLine().ToLower();
                        EquipHelmet(hero, itemChoice);
                        AmountCheck = 0;
                    }
                    else Console.WriteLine("No helmets!");
                    BattleHelper.Anykey();
                    break;
                case "C":
                case "c":
                                        Console.WriteLine(@"
You have ");
                    foreach (string s in hero.Plates)
                    {
                        AmountCheck++;
                        var equipt = s.SanitizeItemName() == hero.Plate;
                        s.PrintItemWithBuffs(sanitizeName: false, isEquipt: equipt);
                    }
                    if (AmountCheck >= 1)
                    {
                        Console.WriteLine("\nWhich plate would you like to equip?");
                        itemChoice = Console.ReadLine().ToLower();
                        EquipPlate(hero, itemChoice);
                    }
                    else Console.WriteLine("No plates!");
                    BattleHelper.Anykey(); 

                    break;
                case "L":
                case "l":
                                        Console.WriteLine(@"
You have ");
                    foreach (string s in hero.Leggings)
                    {
                        AmountCheck++;
                        var equipt = s.SanitizeItemName() == hero.Legging;
                        s.PrintItemWithBuffs(sanitizeName: false, isEquipt: equipt);
                    }
                    if (AmountCheck >= 1)
                    {
                        Console.WriteLine("\nWhich legging would you like to equip?");
                        itemChoice = Console.ReadLine().ToLower();
                        EquipLegging(hero, itemChoice);
                    }
                    else Console.WriteLine("No leggings!");
                    BattleHelper.Anykey();
                    break;
                case "W":
                case "w":
                                        Console.WriteLine(@"
You have ");
                    foreach (string s in hero.Weapons)
                    {
                        AmountCheck++;
                        var equipt = s.SanitizeItemName() == hero.Weapon;
                        s.PrintItemWithBuffs(sanitizeName: false, isEquipt: equipt);
                    }
                    if (AmountCheck >= 1)
                    {
                        Console.WriteLine("\nWhich weapon would you like to equip?");
                        itemChoice = Console.ReadLine().ToLower();
                        EquipWeapons.Equip(hero, itemChoice);
                    }
                    else Console.WriteLine("No weapons!");
                BattleHelper.RefreshStats(hero);
                BattleHelper.Anykey();
                    break;
                case "D":
                case "d":
                    break;
                case "U":
                case "u":
                    hero.Helmet = "";
                    hero.Legging = "";
                    hero.Plate = "";
                    hero.Weapon = "";
                    hero.StrengthBuff = 0;
                    hero.GoldBuff = 0;
                    hero.IntelBuff = 0;
                    hero.HealthBuff = 0;
                    hero.AgilityBuff = 0;
                    hero.HelmetDefense = hero.ChestDefense = hero.LegDefense = 0;
                    BattleHelper.RefreshStats(hero);
                    Console.WriteLine("All items unequipt!");
                    BattleHelper.Anykey();
                    break;
                case "E":
                case "e":
                    Console.WriteLine("\nRight now you have equipt\n");

                    OutputHelper.columnedOutput("Helment:", string.IsNullOrWhiteSpace(hero.Helmet) ? "Nothing" : hero.Helmet, columnWidth: 12);
                    OutputHelper.columnedOutput("Plate:", string.IsNullOrWhiteSpace(hero.Plate) ? "Nothing" : hero.Plate, columnWidth: 12);
                    OutputHelper.columnedOutput("Legging:", string.IsNullOrWhiteSpace(hero.Legging) ? "Nothing" : hero.Legging, columnWidth: 12);
                    OutputHelper.columnedOutput("Weapon:", string.IsNullOrWhiteSpace(hero.Weapon) ? "Nothing" : hero.Weapon, columnWidth: 12);
                    Console.WriteLine();
                    BattleHelper.Anykey();
                    Equip(hero);
                    break;
            }
        }
        public static void EquipPlate(Hero hero, string choice)
        {
                        
                if (choice == "d" && hero.Plates.Contains("(D)irty Plate, "))
                {
                    hero.ChestDefense = 10;
                    hero.Plate = "Dirty Plate";
                    Console.WriteLine("Dirty Plate equipped!");
                }
                else  if (choice == "l" && hero.Plates.Contains("(L)eather Plate, "))
                {
                    hero.Plate = "Leather Plate";
                    hero.ChestDefense = 15;
                    Console.WriteLine("Leather Plate Equipped!");
                }
                else if (choice == "i" && hero.Plates.Contains("(I)ron Plate, "))
                {
                    hero.Plate = "Iron Plate";
                    hero.ChestDefense = 20;
                    Console.WriteLine("Iron Plate Equipped!");
                }
                else if (choice == "a" && hero.Plates.Contains("Di(A)mond Plate, "))
                {
                    hero.Plate = "Diamond Plate";
                    hero.ChestDefense = 25;
                    Console.WriteLine("Diamond Plate Equipped!");
                }
                else if (choice == "s" && hero.Plates.Contains("(S)uper Awesome Plate, "))
                {
                    hero.Plate = "Super Awesome Plate";
                    hero.ChestDefense = 30;
                    Console.WriteLine("Super Awesome Plate Equipped!");
                }
                else if (choice == "f" && hero.Plates.Contains("(F)ortunate Plate, "))
                {
                    hero.Plate = "Fortunate Plate";
                    hero.ChestDefense = 20;
                    hero.GoldBuff = 10;
                    Console.WriteLine("Fortunate Plate Equipped!");
                }
                else 
                {
                    Console.WriteLine("I do not recognise that");
                }
                BattleHelper.RefreshStats(hero);
                BattleHelper.Anykey();
                Equip(hero);
        }
        
        public static void EquipHelmet(Hero hero, string choice)
        {
                if (choice == "d" && hero.Helmets.Contains("(D)irty Cap, "))
                {
                    hero.HelmetDefense = 5;
                    hero.Helmet = "Dirty Cap";
                    Console.WriteLine("Dirty Cap Equipped!");
                }
                else if (choice == "l" && hero.Helmets.Contains("(L)eather Cap, "))
                {
                    hero.Helmet = "Leather Cap";
                    Console.WriteLine("Leather Cap Equipped!");
                    hero.HelmetDefense = 10;
                }
                else if (choice == "i" && hero.Helmets.Contains("(I)ron Cap, "))
                {
                    hero.Helmet = "Iron Cap";
                    Console.WriteLine("Iron Cap Equipped!");
                    hero.HelmetDefense = 13;
                }
                else if (choice == "a" && hero.Helmets.Contains("Di(A)mond Cap, "))
                {
                    hero.Helmet = "Diamond Cap";
                    Console.WriteLine("Diamond Cap Equipped!");
                    hero.HelmetDefense+= 17;
                }
                else if (choice == "s" && hero.Helmets.Contains("(S)uper Awesome Helmet, "))
                {
                    hero.Helmet = "Super Awesome Helmet";
                    Console.WriteLine("Super Awesome Helmet Equipped!");
                    hero.HelmetDefense = 25;
                }
                else 
                {
                    Console.WriteLine("I do not recognise that");
                }
                BattleHelper.RefreshStats(hero);
                BattleHelper.Anykey();
                Equip(hero);
        }
        #region Legs
        public static void EquipLegging(Hero hero, string choice)
        {
                if (choice == "d" && hero.Leggings.Contains("(D)irty Leggings, "))
                {
                    hero.LegDefense = 7;
                    hero.Legging = "Dirty Leggings";
                    Console.WriteLine("Dirty Leggings Equipped!");
                }
                else  if (choice == "l" && hero.Leggings.Contains("(L)eather Leggings, "))
                {
                    hero.Legging = "Leather Leggings";
                    hero.LegDefense = 12;
                    Console.WriteLine("Leather Leggings Equipped!");
                }
                else  if (choice == "i" && hero.Leggings.Contains("(I)ron Leggings, "))
                {
                    hero.Legging = "Iron Leggings";
                    Console.WriteLine("Iron Leggings Equipped!");
                    hero.LegDefense = 15;
                }
                else if (choice == "a" && hero.Leggings.Contains("Di(A)mond Leggings, "))
                {
                    hero.Legging = "Diamond Leggings";
                    hero.LegDefense = 20;
                    Console.WriteLine("Diamond Leggings Equipped!");
                }
                else if (choice == "s" && hero.Leggings.Contains("(S)uper Awesome Leggings, "))
                {
                    hero.Legging = "Super Awesome Leggings";
                    hero.LegDefense = 27;
                    Console.WriteLine("Super Awesome Leggings Equipped!");
                }
                else 
                {
                    Console.WriteLine("I do not recognise that");
                }
                BattleHelper.RefreshStats(hero);
                BattleHelper.Anykey();
                Equip(hero);
        }
        #endregion



    }
}
