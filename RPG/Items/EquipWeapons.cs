﻿using System;

namespace RPG
{
    class EquipWeapons
    {
        public EquipWeapons()
        {

        }
        public static Hero Equip(Hero hero, string choice)
        {
            if (choice == "r" && hero.Weapons.Contains("Di(R)ty Dagger, "))
            {
                hero.Weapon = "Dirty Dagger";
                hero.WeaponAttack = 13;
                Console.WriteLine("Dirty Dagger has been equipt!");
            }
            else if (choice == "h" && hero.Weapons.Contains("(H)atchet, "))
            {
                hero.Weapon = "Hatchet";
                hero.WeaponAttack = 20;
                Console.WriteLine("Hatchet has been equipt!");
            }
            else if (choice == "b" && hero.Weapons.Contains("(B)asic Staff, "))
            {
                hero.Weapon = "Basic Staff";
                hero.WeaponAttack = 20;
                hero.IntelBuff += 1;
                Console.WriteLine("Basic Staff has been equipt!");
            }
            else if (choice == "c" && hero.Weapons.Contains("S(C)imitar, "))
            {
                hero.Weapon = "Scimitar";
                hero.WeaponAttack = 40;
                Console.WriteLine("Scimitar has been equipt!");
            }
            else if (choice == "k" && hero.Weapons.Contains("(K)atana, "))
            {
                hero.Weapon = "Katana";
                hero.WeaponAttack = 50;
                hero.AgilityBuff = 1;
                Console.WriteLine("Katana has been equipt!");
            }
            else if (choice == "a" && hero.Weapons.Contains("(A)wesome Sword, "))
            {
                hero.Weapon = "Awesome Sword";
                hero.WeaponAttack = 70;
                Console.WriteLine("Awesome Sword has been equipt!");
            }
            else if (choice == "m" && hero.Weapons.Contains("(M)anly Hammer, "))
            {
                hero.Weapon = "Manly Hammer";
                hero.WeaponAttack = 80;
                hero.StrengthBuff = 4;
                Console.WriteLine("Manly Hammer has been equipt!");
            }
            else if (choice == "s" && hero.Weapons.Contains("(S)ange and Yasha, "))
            {
                hero.Weapon = "Sange and Yasha";
                hero.WeaponAttack = 90;
                hero.AgilityBuff = 4;
                Console.WriteLine("Sange and Yasha has been equipt!");
            }
            else if (choice == "w" && hero.Weapons.Contains("(W)izard Staff, "))
            {
                hero.Weapon = "Wizard Staff";
                hero.WeaponAttack = 40;
                hero.IntelBuff = 4;
                Console.WriteLine("Wizard Staff has been equipt!");
            }
            else if (choice == "p" && hero.Weapons.Contains("(P)wn Hammer, "))
            {
                hero.Weapon = "Pwn Hammer";
                hero.WeaponAttack = 120;
                hero.AgilityBuff = 3;
                hero.IntelBuff = 3;
                hero.StrengthBuff = 3;
                Console.WriteLine("PWN HAMMER!!!!! has been equipt!");
            }
            else if (choice == "e" && hero.Weapons.Contains("(E)rror Sword, "))
            {
                hero.Weapon = "Error Sword";
                hero.WeaponAttack = 100;
                hero.IntelBuff = 4;
                Console.WriteLine("You feel the power of Ermac");
            }
            else if (choice == "i" && hero.Weapons.Contains("(I)mpowering Sword, "))
            {
                hero.Weapon = "Impowering Sword";
                hero.WeaponAttack = 70;
                hero.HealthBuff = 30;
                Console.WriteLine("You feel slightly stronger..");
            }
            else if (choice == "f" && hero.Weapons.Contains("(F)ire Sword, "))
            {
                hero.Weapon = "Fire Sword";
                hero.WeaponAttack = 140;
                hero.StrengthBuff = 3;
                hero.AgilityBuff = 5;
                hero.IntelBuff = 5;
                Console.WriteLine("Fire sword has been equipt!");
            }
            return hero;
        }
    }
}
