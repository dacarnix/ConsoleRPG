﻿namespace RPG
{
    class Leggings : BaseItem
    {
        public Leggings()
        {
            Cost = 0;
            DefenceRating = 0;
            WearingArea = "Legs";
        }
    }
    class DirtyLeggings : Leggings
    {
        public DirtyLeggings()
        {
            Cost = 40;
            DefenceRating = 7;
            sellValue = 30;
            Identifier = "(D)irty Leggings";
        }
    }
    class LeatherLeggings : Leggings
    {
        public LeatherLeggings()
        {
            Cost = 65;
            DefenceRating = 12;
            sellValue = 75;
            Identifier = "(L)eather Leggings";
        }
    }
    class IronLeggings : Leggings
    {
        public IronLeggings()
        {
            Cost = 110;
            DefenceRating = 15;
            sellValue = 130;
            Identifier = "(I)ron Leggings";
        }
    }
    class DiamondLeggings : Leggings
    {
        public DiamondLeggings()
        {
            Cost = 140;
            DefenceRating = 20;
            sellValue = 225;
            Identifier = "Di(A)mond Leggings";
        }
    }
    class SuperAwesomeLeggings : Leggings
    {
        public SuperAwesomeLeggings()
        {
            Cost = 200;
            DefenceRating = 27;
            sellValue = 0;
            Identifier = "(S)uper Awesome Leggings";
        }
    }
}
