﻿namespace RPG
{
    class Helmets : BaseItem
    {
        public Helmets()
        {
            Cost = 0;
            DefenceRating = 0;
            WearingArea = "Head";
        }
    }
    class DirtyCap : Helmets 
    {
        public DirtyCap()
        {
            Cost = 20; // might need base.
            DefenceRating = 5;
            Identifier = "(D)irty Cap";
        }
    }
    class LeatherHelmet : Helmets
    {
        public LeatherHelmet()
        {
            Cost = 50;
            DefenceRating = 10;
            Identifier = "(L)eather Cap";
        }
    }
    class IronHelmet : Helmets
    {
        public IronHelmet()
        {
            Cost = 80;
            DefenceRating = 13;
            Identifier = "(I)ron Cap";
        }
    }
        class DiamondHelmet : Helmets
    {
        public DiamondHelmet()
        {
            Cost = 120;
            DefenceRating = 17;
            Identifier = "Di(A)mond Cap";
        }
    }
        class PureAwesomeHelmet : Helmets
    {
        public PureAwesomeHelmet()
        {
            Cost = 150;
            sellValue = 0;
            DefenceRating = 25;
            Identifier = "(S)uper Awesome Helmet";
        }
    }

}

