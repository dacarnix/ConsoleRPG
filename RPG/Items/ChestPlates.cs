﻿namespace RPG
{
    class ChestPlates : BaseItem
    {
        public ChestPlates()
        {
            WearingArea = "Chest";
        }
    }
    class DirtyPlate : ChestPlates
    {
        public DirtyPlate()
        {
            Cost = 50;
            DefenceRating = 10;
            Identifier = "(D)irty Plate";
            sellValue = 40;
        }
    }
    class LeatherPlate : ChestPlates
    {
        public LeatherPlate()
        {
            Cost = 90;
            DefenceRating = 15;
            Identifier = "(L)eather Plate";
            sellValue = 90;
        }
    }
    class IronPlate : ChestPlates
    {
        public IronPlate()
        {
            Cost = 140;
            DefenceRating = 20;
            Identifier = "(I)ron Plate";
            sellValue = 140;
        }
    }
    class DiamondPlate : ChestPlates
    {
        public DiamondPlate()
        {
            Cost = 200;
            DefenceRating = 25;
            Identifier = "Di(A)mond Plate";
            sellValue = 250;
        }
    }
    class SuperMegaAwesomePlate : ChestPlates
    {
        public SuperMegaAwesomePlate()
        {
            Cost = 250;
            DefenceRating = 30;
            Identifier = "(S)uper Awesome Plate";
            sellValue = 0;
        }
    }
    class Fortunate : ChestPlates
    {
        public Fortunate()
        {
            Cost = 0;
            DefenceRating = 20;
            Identifier = "(F)ortunate Plate";
        }
    }
}
