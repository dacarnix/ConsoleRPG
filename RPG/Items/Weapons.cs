﻿namespace RPG
{
    class Weapons : BaseItem
    {
        public Weapons()
        {
            WearingArea = "Arm";
        }
    }
    class DirtyDagger : Weapons
    {
        public DirtyDagger()
        {
            Cost = 40;
            AttackDamage = 13;
            Identifier = "Di(R)ty Dagger";
        }
    }
    class Hatchet : Weapons
    {
        public Hatchet()
        {
            Cost = 60;
            AttackDamage = 20;
            Identifier = "(H)atchet";
        }
    }
    class BasicStaff : Weapons
        {
            public BasicStaff()
            {
                Cost = 70;
                AttackDamage = 5;
                IntelligenceBuff = 1;
                Identifier = "(B)asic Staff";
            }
        }
    class Scimitar : Weapons
    {
            public Scimitar()
            {
                Cost = 100;
                AttackDamage = 40;
                Identifier = "S(C)imitar";
            }
    }
    class Katana : Weapons
    {
            public Katana()
            {
                Cost = 130;
                AttackDamage = 50;
                AgilityBuff = 1;
                Identifier = "(K)atana";
            }
    }
    class AwesomeSword : Weapons
    {
            public AwesomeSword()
            {
                Cost = 160;
                AttackDamage = 70;
                Identifier = "(A)wesome Sword";
            }
    }
    class ManlyHammer : Weapons
    {
            public ManlyHammer()
            {
                Cost = 200;
                AttackDamage = 80;
                StrengthBuff = 4;
                Identifier = "(M)anly Hammer";
            }
    }
    class WizardStaff : Weapons
    {
            public WizardStaff()
            {
                Cost = 200;
                AttackDamage = 40;
                IntelligenceBuff = 4;
                Identifier = "(W)izard Staff";
            }
    }
    class SangeAndYasha : Weapons
    {
            public SangeAndYasha()
            {
                Cost = 200;
                AttackDamage = 90;
                AgilityBuff = 3;
                Identifier = "(S)ange and Yasha";
            }
    }
    class PwnHammer : Weapons
    {
           public PwnHammer()
             {
                 Cost = 270;
                 AttackDamage = 120;
                 StrengthBuff = 3;
                 AgilityBuff = 3;
                 IntelligenceBuff = 3;
               Identifier = "(P)wn Hammer";

             }
    }
    class ErrorSword : Weapons
    {
        public ErrorSword()
        {
            Cost = 0;
            AttackDamage = 100;
            Identifier = "(E)rror Sword";
        }
    }
    class ImpoweringSword : Weapons
    {
        public ImpoweringSword()
        {
            Cost = 0;
            AttackDamage = 70;
            Identifier = "(I)mpowering Sword";
            HealthBuff = 30;
        }
    }
    class Fire_Blade : Weapons
    {
        public Fire_Blade()
        {
            Cost = 0;
            AttackDamage = 140;
            StrengthBuff = 3;
            AgilityBuff = 5;
            IntelligenceBuff = 5;
            Identifier = "(F)ire Sword";
        }
    }
}
