﻿using System;

namespace RPG
{
    class ShopHelper
    {
        public ShopHelper()
        {
           
        }
        public static Hero helmetBuy(Hero hero)
        {
            string choice ;
            DirtyCap dirt = new DirtyCap();
            LeatherHelmet leat = new LeatherHelmet();
            IronHelmet iron = new IronHelmet();
            DiamondHelmet diam = new DiamondHelmet();
            Console.WriteLine(@"
What helmet would you like to buy?

You have {8} gold

Name           Cost        Defense Rating
________________________________________
(D)irty Cap    {0}          {1}
(L)eather Cap  {2}          {3}
(I)ron Cap     {4}          {5}
Di(A)mond Cap  {6}          {7}
????           ????        ????

G(E)t me the hell outta here!
________________________________________", dirt.Cost, dirt.DefenceRating, leat.Cost, leat.DefenceRating, iron.Cost,
                             iron.DefenceRating, diam.Cost, diam.DefenceRating,hero.Gold);
            choice = Console.ReadLine();
            switch (choice)
            {
                case "D":
                case "d":
                    ShopCheck.checkBuy(dirt, hero);  
                    return hero;
                case "L":
                case "l":
                    ShopCheck.checkBuy(leat, hero);
                    return hero;

                case "I":
                case "i":
                    ShopCheck.checkBuy(iron, hero);
                    return hero;
                case "A":
                case "a":
                    ShopCheck.checkBuy(diam, hero);
                    return hero;
                case "e":
                case "E":
                    Console.WriteLine("Thank you for your time");
                    BattleHelper.Anykey();
                    break;
                default:
                    Console.WriteLine("Sorry I dont recognise that.. Trolling fool");
                    helmetBuy(hero);
                    break;
            }
            return hero;
            
        }

                public static Hero PlateBuy(Hero hero)
        {
            string choice ;
            DirtyPlate dirt = new DirtyPlate();
            LeatherPlate leat = new LeatherPlate();
            IronPlate iron = new IronPlate();
            DiamondPlate diam = new DiamondPlate();
            Console.WriteLine(@"
What plate would you like to buy?

You have {8} gold

Name           Cost        Defense Rating
________________________________________
(D)irty Plate     {0}         {1}
(L)eather Plate   {2}         {3}
(I)ron Plate      {4}         {5}
Di(A)mond Plate   {6}         {7}

G(E)t me the hell outta here!
________________________________________", dirt.Cost, dirt.DefenceRating, leat.Cost, leat.DefenceRating, iron.Cost,
                             iron.DefenceRating, diam.Cost, diam.DefenceRating,hero.Gold);
            choice = Console.ReadLine().ToLower();
            switch (choice)
            {
                case "d":
                    ShopCheck.checkBuy(dirt, hero);  
                    return hero;
                case "l":
                    ShopCheck.checkBuy(leat, hero);
                    return hero;
                case "i":
                    ShopCheck.checkBuy(iron, hero);
                    return hero;
                case "a":
                    ShopCheck.checkBuy(diam, hero);
                    return hero;
                case "e":
                    Console.WriteLine("Thank you for your time");
                    BattleHelper.Anykey();
                    break;
                default:
                    Console.WriteLine("Sorry I dont recognise that.. Trolling fool");
                    PlateBuy(hero);
                    break;
            }
            return hero;
            
        }

                public static Hero LeggingsBuy(Hero hero)
                {
                    string choice;
                    DirtyLeggings dirt = new DirtyLeggings();
                    LeatherLeggings leat = new LeatherLeggings();
                    IronLeggings iron = new IronLeggings();
                    DiamondLeggings diam = new DiamondLeggings();
                    Console.WriteLine(@"
What legging would you like to buy?

You have {8} gold

Name                Cost        Defense Rating
_____________________________________________
(D)irty Leggings     {0}         {1}
(L)eather Leggings   {2}         {3}
(I)ron Leggings      {4}         {5}
Di(A)mond Leggings   {6}         {7}

G(E)t me the hell outta here!
_____________________________________________", dirt.Cost, dirt.DefenceRating, leat.Cost, leat.DefenceRating, iron.Cost,
                                     iron.DefenceRating, diam.Cost, diam.DefenceRating,hero.Gold);
                    choice = Console.ReadLine().ToLower();
                    switch (choice)
                    {
                        case "d":
                            ShopCheck.checkBuy(dirt, hero);
                            return hero;
                        case "l":
                            ShopCheck.checkBuy(leat, hero);
                            return hero;
                        case "i":
                            ShopCheck.checkBuy(iron, hero);
                            return hero;
                        case "a":
                            ShopCheck.checkBuy(diam, hero);
                            return hero;
                        case "e":
                            Console.WriteLine("Thank you for your time");
                            BattleHelper.Anykey();
                            break;
                        default:
                            Console.WriteLine("Sorry I dont recognise that.. Trolling fool");
                            LeggingsBuy(hero);
                            break;
                    }
                    return hero;

                }
                public static Hero WeaponsBuy(Hero hero)
                {
                    string choice;
                    DirtyDagger dirt = new DirtyDagger();
                    Hatchet leat = new Hatchet();
                    BasicStaff basic = new BasicStaff();
                    Scimitar scim = new Scimitar();
                    Katana kat = new Katana();
                    AwesomeSword awe = new AwesomeSword();
                    Console.WriteLine(@"
What weapon would you like to buy?

You have {14} gold

Name                Cost        Attack      StatBuff
_______________________________________________________
Di(R)ty Dagger       {0}         {1}
(H)atchet            {2}         {3}
(B)asic Staff        {4}         {5}        {6}
S(C)imitar           {7}         {8}
(K)atana             {9}         {10}       {11}
(A)wesome Sword      {12}        {13}

G(E)t me the hell outta here!
________________________________________________________", dirt.Cost, dirt.AttackDamage, leat.Cost, leat.AttackDamage, basic.Cost, basic.AttackDamage, "INT + " + basic.IntelligenceBuff
                                                         , scim.Cost, scim.AttackDamage, kat.Cost, kat.AttackDamage, "AGI + " + kat.AgilityBuff, awe.Cost, awe.AttackDamage,hero.Gold);
                    choice = Console.ReadLine().ToLower();
                    switch (choice)
                    {
                        case "r":
                            ShopCheck.checkBuy(dirt, hero);
                            return hero;
                        case "h":
                            ShopCheck.checkBuy(leat, hero);
                            return hero;
                        case "b":
                            ShopCheck.checkBuy(basic, hero);
                            return hero;
                        case "c":
                            ShopCheck.checkBuy(scim, hero);
                            return hero;
                        case "k":
                            ShopCheck.checkBuy(kat, hero);
                            break;
                        case "a":
                            ShopCheck.checkBuy(awe, hero);
                            break;
                        case "e":
                            Console.WriteLine("Thank you for your time");
                            BattleHelper.Anykey();
                            break;
                        default:
                            Console.WriteLine("Sorry I dont recognise that.. Trolling fool");
                            WeaponsBuy(hero);
                            break;
                }
                    return hero; 
                }


    }
}
