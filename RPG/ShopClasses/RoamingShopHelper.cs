﻿using System;

namespace RPG
{
    class RoamingShopHelper
    {
        public RoamingShopHelper()
        {

        }
        public static Hero helmetBuy(Hero hero)
        {
            string choice;
            PureAwesomeHelmet sup = new PureAwesomeHelmet();
            Console.WriteLine(@"
What helmet would you like to buy?

You have {2} gold

Name                    Cost        Defense Rating
_______________________________________________
(S)uper Awesome Helmet  {0}         {1}       

G(E)t me the hell outta here!
_______________________________________________", sup.Cost, sup.DefenceRating, hero.Gold);
            choice = Console.ReadLine().ToLower();
            switch (choice)
            {
                case "s":
                    ShopCheck.checkBuy(sup, hero);
                    return hero;
                case "e":
                    Console.WriteLine("Thank you for your time");
                    BattleHelper.Anykey();
                    break;
                default:
                    Console.WriteLine("Sorry I dont recognise that.. Trolling fool");
                    helmetBuy(hero);
                    break;
            }
            return hero;

        }
        
        public static Hero PlateBuy(Hero hero)
        {
            string choice;
            SuperMegaAwesomePlate sup = new SuperMegaAwesomePlate();
            Console.WriteLine(@"
What plate would you like to buy?

You have {2} gold

Name                  Cost        Defense Rating
_______________________________________________
(S)uper Awesome Plate {0}         {1}       

G(E)t me the hell outta here!
_______________________________________________", sup.Cost,sup.DefenceRating, hero.Gold);
            choice = Console.ReadLine().ToLower();
            switch (choice)
            {
                case "s":
                    ShopCheck.checkBuy(sup, hero);
                    return hero;
                case "e":
                    Console.WriteLine("Thank you for your time");
                    BattleHelper.Anykey();
                    break;
                default:
                    Console.WriteLine("Sorry I dont recognise that.. Trolling fool");
                    PlateBuy(hero);
                    break;
            }
            return hero;

        }

        public static Hero LeggingsBuy(Hero hero)
        {
            string choice;
           SuperAwesomeLeggings sup = new SuperAwesomeLeggings();
            Console.WriteLine(@"
What legging would you like to buy?

You have {2} gold

Name                  Cost        Defense Rating
_______________________________________________
(S)uper Awesome Legs  {0}         {1}       

G(E)t me the hell outta here!
_______________________________________________", sup.Cost, sup.DefenceRating, hero.Gold);
            choice = Console.ReadLine().ToLower();
            switch (choice)
            {
                case "s":
                    ShopCheck.checkBuy(sup, hero);
                    return hero;
                case "e":
                    Console.WriteLine("Thank you for your time");
                    BattleHelper.Anykey();
                    break;
                default:
                    Console.WriteLine("Sorry I dont recognise that.. Trolling fool");
                    LeggingsBuy(hero);
                    break;
            }
            return hero;

        }
        public static Hero WeaponsBuy(Hero hero)
        {
            string choice;
            ManlyHammer man = new ManlyHammer();
            SangeAndYasha sange = new SangeAndYasha();
            WizardStaff wiz = new WizardStaff();
            PwnHammer pwn = new PwnHammer();
            Console.WriteLine(@"
What plweaponate would you like to buy?

You have {12} gold

Name                Cost        Attack      Stat Buff
_______________________________________________________
(M)anly Hammer       {0}        {1}         {2}
(S)ange and Yasha    {3}        {4}         {5}
(W)izard Staff       {6}        {7}         {8}
(P)wn Hammer         {9}        {10}        {11}

G(E)t me the hell outta here!
________________________________________________________", man.Cost, man.AttackDamage,
                                                 "STR + " + man.StrengthBuff, sange.Cost, sange.AttackDamage, "AGI + " + sange.AgilityBuff, wiz.Cost, wiz.AttackDamage, "INT + " + wiz.IntelligenceBuff,
                                                 pwn.Cost, pwn.AttackDamage, "STR, AGI, INT + " + pwn.StrengthBuff, hero.Gold);
            choice = Console.ReadLine().ToLower();
            switch (choice)
            {
                case "w":
                    ShopCheck.checkBuy(wiz, hero);
                    break;
                case "s":
                    ShopCheck.checkBuy(sange, hero);
                    break;
                case "m":
                    ShopCheck.checkBuy(man, hero);
                    break;
                case "p":
                    ShopCheck.checkBuy(pwn, hero);
                    break;
                case "e":
                    Console.WriteLine("Thank you for your time");
                    BattleHelper.Anykey();
                    break;
                default:
                    Console.WriteLine("Sorry I dont recognise that.. Trolling fool");
                    WeaponsBuy(hero);
                    break;
            }
            return hero;
        }


    }
}
