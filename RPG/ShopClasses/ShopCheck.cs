﻿using System;

namespace RPG
{
    class ShopCheck : ShopHelper
    {
        public ShopCheck()
        {

        }
        public static Hero checkBuy(BaseItem helmet, Hero hero)
        {
            if (hero.Helmets.Contains(helmet.Identifier + ", "))
            {
                Console.WriteLine("You already own that!");
                return hero;
            }
            else if (hero.Plates.Contains(helmet.Identifier + ", "))
            {
                Console.WriteLine("You already own that!");
                return hero;
            }
            else if (hero.Leggings.Contains(helmet.Identifier + ", "))
            {
                Console.WriteLine("You already own that!");
                return hero;
            }
            else if (hero.Weapons.Contains(helmet.Identifier + ", "))
            {
                Console.WriteLine("You already own that!");
                return hero;
            }
            if (helmet.Cost <= hero.Gold)
            {
                hero.Gold -= helmet.Cost;
                if (helmet.WearingArea == "Head") hero.Helmets.Add(helmet.Identifier + ", ");
                else if (helmet.WearingArea == "Chest") hero.Plates.Add(helmet.Identifier + ", ");
                else if (helmet.WearingArea == "Legs") hero.Leggings.Add(helmet.Identifier + ", ");
                else if (helmet.WearingArea == "Arm") hero.Weapons.Add(helmet.Identifier + ", ");
Console.WriteLine(@"
Thank you for buying {0}!
You can equip it in the Eq(U)ip in the menu", helmet.Identifier);
BattleHelper.Anykey();

            }
            else Console.WriteLine("Sorry you can not afford that");
            return hero;
        }
    }
}
