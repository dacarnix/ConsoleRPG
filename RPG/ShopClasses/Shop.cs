﻿using System;

namespace RPG
{

    class Shop

        {
            string choice = "";
            public Shop(Hero hero)
            {
                Console.Clear();
                Console.WriteLine("Welcome to the Shop!\n");
                Console.WriteLine("You have {0} gold",hero.Gold);
                ShopLoop(hero);
            }
            public void ShopLoop(Hero hero)
            {
                do
                {
                    Console.WriteLine("What would you like to buy?");
                    Console.Write(@"
(H)elmets
(C)hestplates
(L)eggings
(W)eapons
(P)otions

(D)one
");
                    Console.WriteLine();
                    choice = Console.ReadLine().ToLower();
                    switch (choice)
                    {
                        case "h":
                            Console.Clear();
                            if (hero.isRoaming == true) RoamingShopHelper.helmetBuy(hero);
                            else ShopHelper.helmetBuy(hero);
                            break;
                        case "c":
                            Console.Clear();
                            if (hero.isRoaming == true) RoamingShopHelper.PlateBuy(hero);
                            else ShopHelper.PlateBuy(hero);
                            break;
                        case "l":
                            Console.Clear();
                            if (hero.isRoaming == true) RoamingShopHelper.LeggingsBuy(hero);
                            else ShopHelper.LeggingsBuy(hero);
                            break;
                        case "w":
                            if (hero.isRoaming == true) RoamingShopHelper.WeaponsBuy(hero);
                            else ShopHelper.WeaponsBuy(hero);
                            break;
                        case "p":
                            Console.Clear();
                            Console.WriteLine("Hello! What potion would you like to buy?\n\nYou can only have 3 of each type at most.");
                            Console.WriteLine("You currently have {0} health potion(s) and {1} mana potion(s)\n", hero.HealthPotions, hero.ManaPotions);
                            OutputHelper.columnedOutput("Type", "Cost", "Restores", columnWidth: 12);
                            OutputHelper.columnedOutput("(H)ealth", "20", "200HP", columnWidth: 12);
                            OutputHelper.columnedOutput("(M)ana", "15", "100MP", columnWidth: 12);
                            Console.WriteLine();
                            Console.WriteLine("(L)eave");
                            string potionChoice = Console.ReadLine().ToLower();
                            switch (potionChoice)
                            {
                                case "h" :
                                    if (hero.Gold >= 20)
                                    {
                                        if (hero.HealthPotions == 3) Console.WriteLine("You already have the full amount of 3 health potions!");
                                        else
                                        {
                                            hero.HealthPotions += 1;
                                            hero.Gold -= 20;
                                            Console.WriteLine("Thank you! You now have {0} health pot(s)!", hero.HealthPotions.ToString());
                                        }
                                    }
                                    else Console.WriteLine("You can not afford that!");
                                    break;
                                case "m":
                                    if (hero.Gold >= 15)
                                    {
                                        if (hero.ManaPotions == 3) Console.WriteLine("You already have the full amount of 3 mana potions!");
                                        else
                                        {
                                            hero.ManaPotions += 1;
                                            hero.Gold -= 20;
                                            Console.WriteLine("Thank you! You now have {0} mana pot(s)!", hero.ManaPotions.ToString());
                                        }
                                    }
                                    else Console.WriteLine("You can not afford that!");
                                    break;
                               default:
                                break;
                            }
                            break;
                        case "d":
                            Console.WriteLine("Goodbye, and be careful out there!");
                            break;
                        default:
                            Console.WriteLine("Stop trolling me fool and type something proper!");
                            break;
                    }

                }
                while (choice != "d");
                BattleHelper.Anykey();
            }
        }

    }

