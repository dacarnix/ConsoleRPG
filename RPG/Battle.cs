﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG
{
    class Battle
    {
        Random Rand;
        string playerChoice;
        int Damage;
        string monsterchoice;
        public Battle(Hero hero, Monster monster)
        {
            Console.WriteLine("{0} is facing a {1}.", hero.Identifier, monster.Identifier);
            BattleLoop(hero, monster);
        }
         public void BattleLoop(Hero hero, Monster monster)
        {

            do
            {
                monster.isAlive = CheckHealth(monster.CurrentHealth);
                PrintStatus(hero,monster);
                playerChoice = PrintChoice();
                Console.WriteLine();
                
                hero.isAlive = CheckHealth(hero.CurrentHealth);
                
                ProcessDamage(playerChoice, hero, monster);
               if (monster.isAlive == true)
                {
                    monsterchoice = monster.AI();
                    ProcessDamage(monsterchoice, monster,hero);
                }
                Console.ReadLine();
                Console.Clear();

            }
            while (hero.isAlive == true && monster.isAlive == true);
        }

        public void ProcessDamage(string choice, Characters attacker, Characters defender)
        {
            switch(choice)
            {
                case "A":
                case "a":
                Console.WriteLine();
                Console.WriteLine("{0} Attacks!",attacker.Identifier);
                DealDamage(attacker,defender);
                defender.CurrentHealth -= Damage;
                Console.WriteLine();
                Console.WriteLine("{0} attacks {1} with {2} damage!", attacker.Identifier,defender.Identifier,Damage);
                 break;
                case "D":
                case "d":
                 Console.WriteLine();
                 Console.WriteLine("{0} Defends!", attacker.Identifier);
                    // add defending methods  and values etc
                 break;
                case "F":
                case "f":
                 Console.WriteLine();
                 Console.WriteLine("{0} has Ran away!", attacker.Identifier);
                    
                 break;
                case "S":
                case "s":
                        Console.WriteLine();
                        Console.WriteLine("{0} has used a spell upon you!", attacker.Identifier);
                        DealDamage(attacker, defender);
                        break;
                    
                default:
                 Console.WriteLine();
                 Console.WriteLine("That letter is invalid, please try again");
                 choice = PrintChoice();
                 ProcessDamage(choice, attacker, defender);
                 break;


            }
        }
        public void CheckDefence(string choice, Characters attacker)
        {
            if (attacker.Defending == true) attacker.IncreasedAttack = true;
            else attacker.IncreasedAttack = false;
            if (choice == "d" || choice == "D") attacker.Defending = true;
            else attacker.Defending = false;

        }
        public void PrintStatus(Hero hero, Monster monster)
        {

            Console.Write(@"

********************************
      HP/MaxHP   MP/MaxMP

{0}:  {1}/{2}hp    {3}/{4}mp

{5}: {6}/{7}hp      {8}/{9}mp

********************************

",       hero.Identifier, hero.CurrentHealth, hero.MaxHealth, hero.CurrentMagic, hero.MaxMagic,
         monster.Identifier, monster.CurrentHealth, monster.MaxHealth, monster.CurrentMagic,monster.MaxMagic);

        }

        int DealDamage(Characters attacker, Characters defender)
        {
            int min;
            Rand = new Random();
            int max = (attacker.AttackDamage - defender.Defense);
            if (max <= 0) max = 1; // so it wont do negative or anything damage. :)
            min = ((int)(attacker.AttackDamage * .8) - defender.Defense);
            Damage = Rand.Next(min, max);
            if (defender.Defending == true) Damage = (Damage / 2);
            if (attacker.IncreasedAttack == true) Damage = (int)(Damage * 1.5);
            return Damage;
        }
    

        string PrintChoice()
        {
            string choice;
            Console.WriteLine();
            Console.Write(@"
        Please choose an action:

        (A)ttack:

        (D)efend:
        ");
            Console.WriteLine();
            choice = Console.ReadLine();
            return choice;
        }
        bool CheckHealth(int health)
        {
            bool Alive = true;
            if (health <= 0) Alive = false;
            if (health > 0) Alive = true;
            return Alive;
        }
        
    

    

    }
}
    


