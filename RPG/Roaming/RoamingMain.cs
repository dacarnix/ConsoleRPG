﻿using System;

namespace RPG
{
    class RoamingMain
    {
        bool FalseValue = false;
        public RoamingMain(Hero hero)
        {
            
            if (hero.roamingKey == false)
            {
                Console.Write("You do not have the rights to be out here!\nYou must first defeat our boss.");
                Console.WriteLine();
                BattleHelper.Anykey();
                Console.WriteLine("Welcome back!");
                MainGame main = new MainGame(FalseValue, hero);
            }
            else RoamingStatus(hero);
        }
        public void RoamingStatus(Hero hero)
        {
            Console.Clear();
            string Choice;
            Console.WriteLine(@"Are you sure you would like to go (R)oaming?
Or would you like to go back to your (V)illage?

You can only return to your village after 3 rounds (fighting) encountered during (r)oaming.
You cannot flee from battles that occur while roaming.", hero.Identifier);
            Console.Write("\n(R/V): ");
            Choice = Console.ReadLine().ToLower();
            switch (Choice)
            {
                case "r":
                    hero.isRoaming = true;
                    Roaming r = new Roaming(hero);
                    break;
                case "v":
                    Console.WriteLine("\nOkay then, take care!\n");

                    BattleHelper.Anykey();
                    if (hero.secretVillage)
                    {
                        NewVillageMain vm = new NewVillageMain(hero);
                    }
                    else
                    {
                        MainGame main = new MainGame(FalseValue, hero);
                    }
                    break;
                default:
                    Console.WriteLine("I didnt get that");
                    RoamingStatus(hero);
                    BattleHelper.Anykey();
                    break;
            }

        }


    }
}
