﻿using System;
using System.Collections.Generic;

namespace RPG
{

    class Roaming
    {
        List<Characters> charList;
        int choice;
        int RoamingDecision;
        public Roaming(Hero hero)
        {
            for (int i = 0; i < 3; i++)
            {
                Console.Clear();
                charList = new List<Characters>();

                RoamingDecision = OutputHelper.rand.Next(1, 13);
                if (RoamingDecision >= 0 && RoamingDecision <= 8 || (RoamingDecision > 10 && RoamingDecision <= 13 && i == 0))
                {
                    RandomNextPerson(hero);
                    RoamingBattle rb = new RoamingBattle(hero, charList);
                    if (hero.isAlive == false)
                    {
                        if (hero.Gold >= 15)
                        {
                            hero.Gold -= 15;
                            hero.CurrentHealth = hero.MaxHealth;
                            hero.CurrentMagic = hero.MaxMagic;
                            Console.WriteLine(@"
You wake up in your villages inn after a serious fight.
You have been revived for 15 gold");
                            BattleHelper.Anykey();
                            hero.isAlive = true;
                            bool b = false;
                            MainGame main = new MainGame(b, hero);
                            break;
                        }
                    }

                    #region Exp
                    int gold = 0;
                    int experience = 0;
                    foreach (Characters monster in charList)
                    {
                        if (monster.fled == false)
                        {
                            experience += monster.Experience;
                            gold += monster.Gold;
                        }
                    }
                    Console.WriteLine("{0} gets {1} gold and {2} experience", hero.Identifier, gold, experience);
                    hero.Experience += experience;
                    hero.Gold += gold;
                    if (hero.Experience >= (hero.Level * 8 + 5))
                    {
                        hero.Level += 1;
                        hero.SkillPoints += 1;
                        hero.Experience = 0;
                        Console.WriteLine(@"
Congratz! You have leveled up to level {0}!
Go to the main menu to upgrade
the extra skill point you just earned!
", hero.Level);
                    }
                    charList.Clear();
                    BattleHelper.Anykey();
                    #endregion
                }
                else if (RoamingDecision > 8 && RoamingDecision <= 10)
                {
                    RoamingQuotes.Loot(hero);
                }
                else if (RoamingDecision > 10 && RoamingDecision <= 13 && hero.secretVillage != true)
                {
                    Console.WriteLine("You stumble upon a secret village hidden in the trees!");
                    BattleHelper.Anykey();
                    NewVillageMain newVil = new NewVillageMain(hero);
                    break;
                }
                else
                {
                    if (RoamingDecision >= 0 && RoamingDecision <= 8)
                    {
                        RandomNextPerson(hero);
                        RoamingBattle rb = new RoamingBattle(hero, charList);
                        BattleHelper.expCheck(hero);
                    }
                }
                hero.secretVillage = false;
            }

            RoamingMain rm = new RoamingMain(hero);
        }
        #region RandomSelectEnemy
        public void RandomNextPerson(Hero hero)
        {
            Random rand = new Random();
            choice = OutputHelper.rand.Next(1, 100);
            int randomAddChar = rand.Next(1, 7); // sees or not to vs multpile enemies

            if (choice >= 0 && choice <= 20) charList.Add(new Slime());
            else if (choice >= 21 && choice <= 50) charList.Add(new Troll(hero));
            else if (choice >= 51 && choice <= 70) charList.Add(new NightRouge(hero));
            else if (choice >= 71 && choice <= 85) charList.Add(new ElfMage(hero));
            else if (choice >= 86 && choice <= 100) charList.Add(new Mage());
            else charList.Add(new Slime());
            if (randomAddChar == 1) charList.Add(new Slime());
            else if (randomAddChar == 2) charList.Add(new Troll(hero));
            else if (randomAddChar == 3) charList.Add(new Rouge());

        }
        #endregion
    }
}
