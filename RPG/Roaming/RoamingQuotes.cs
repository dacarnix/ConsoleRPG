﻿using System;

namespace RPG
{
    class RoamingQuotes
    {
       static int randomChoice;
        public RoamingQuotes()
        {

        }
        #region Random1Enemy
        public static string RandomQuoteEnemy()
        {
            string choice = "";
            randomChoice = OutputHelper.rand.Next(1, 7);
            switch (randomChoice)
            {
                case 1:
                    choice = @"You have stumbled into a small forest.
Where a wild {0} appears!
";
                    return choice;
                case 2:
                    choice = @"A chill goes down your spine
as a {0} appears!
";
                    return choice;
                case 3:
                    choice = @"You stumble accross a wild {0}!
";
                    return choice;
                case 4:
                    choice = @"A {0} has appeared!
";
                    return choice;
                case 5:
                    choice = @"Out of a nearby bush a {0} appears!
";
                    return choice;
                case 6:
                    choice = @"A angry {0} runs your way!
";
                    return choice;
                default:
                    choice = @"A angry {0} runs your way!
";
                    return choice;
            }
           
        }
        #endregion
        #region MultipleEnemies
        public static string RandomEnemies()
        {
            randomChoice = OutputHelper.rand.Next(1, 4);
            string choice = "";
            switch (randomChoice)
            {
                case 1:
                    choice = @"A {0} and a {1} have appeared!
";
                    return choice;
                case 2:
                    choice = @"You stumble into a pair of Enemies!
";
                    return choice;
                case 3:
                    choice = @"A angry {0} and {1} run your way!
";
                    return choice;
                default:
                    choice = @"You stumble into a pair of Enemies!
";
                    return choice;
            }

        }
        #endregion
        #region Loot
        public static void Loot(Hero hero)
        {
            int goldAmount;
            int trapDamage;
            randomChoice = OutputHelper.rand.Next(1, 7);
            Console.WriteLine("You stumble into a chest!");
            switch (randomChoice)
            {
                case 1:
                    if (hero.HealthPotions == 3) Console.WriteLine("Health potion wont fit!");
                    else
                    {
                        Console.WriteLine("where you find 1 health potion!");
                        hero.HealthPotions++;
                    }
                    BattleHelper.Anykey();
                    break;
                case 2:
                    if (hero.ManaPotions == 3) Console.WriteLine("Health potion wont fit!");
                    else
                    {
                        Console.WriteLine("where you find 1 mana potion!");
                        hero.ManaPotions++;
                    }
                    BattleHelper.Anykey();
                    break;
                case 3:
                   goldAmount = OutputHelper.rand.Next(20, 30);
                    Console.WriteLine("where you find {0} gold!",goldAmount.ToString());
                    hero.Gold += goldAmount;
                    BattleHelper.Anykey();
                    break;
                case 4:
                    Console.WriteLine("where you find nothing useful inside");
                    BattleHelper.Anykey();
                    break;
                case 5:
                    trapDamage = OutputHelper.rand.Next(15,25);
                    Console.WriteLine("when a trap attacks you and hits for {0}hp",trapDamage.ToString());
                    hero.CurrentHealth -= trapDamage;
                    BattleHelper.HeroDeathVoid(hero);
                    BattleHelper.Anykey();
               
                    break;
                case 6:
                    if (!hero.Leggings.Contains("(I)ron Leggings, "))
                    {           
                        Console.WriteLine("where you find iron leggings!");
                        BattleHelper.ItemLoot(new IronLeggings(), hero);
                    }
                    else Console.WriteLine("Iron leggings won't fit! You already have this item");
                    BattleHelper.Anykey();
                    break;
                default:
                    goldAmount = OutputHelper.rand.Next(20, 30);
                    Console.WriteLine("where you find {0} gold!",goldAmount.ToString());
                    hero.Gold += goldAmount;
                    BattleHelper.Anykey();
                    break;
            }
        }
        #endregion
    }
}
