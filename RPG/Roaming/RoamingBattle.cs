﻿using System;
using System.Collections.Generic;

namespace RPG
{
    class RoamingBattle
    {
        string userspellchoice;
        string userchoice;
        string monsterchoice;
        string monsterspellchoice;
        int Counter;
        string Quote;
        bool amonsterleft;
        int targetmonster;

        public RoamingBattle(Hero hero, List<Characters> monsters)
        {
            Console.Clear();
            foreach (Characters monster in monsters)
            {
                Counter++;
            }
            if (Counter == 1)
            {
                Quote = RoamingQuotes.RandomQuoteEnemy();
                Console.WriteLine(Quote, monsters[0].Identifier);
            }
            else if (Counter == 2)
            {
                Quote = RoamingQuotes.RandomEnemies();
                Console.WriteLine(Quote, monsters[0].Identifier, monsters[1].Identifier);
            }
            BattleHelper.Anykey();
            BattleLoop(hero, monsters);
        }



        public void BattleLoop(Hero hero, List<Characters> monsters)
        {
            do
            {
                targetmonster = 0;
                hero.isAlive = BattleHelper.CheckHealth(hero.CurrentHealth);
                if (hero.isAlive == false)
                {
                    if (hero.Gold >= 15)
                    {
                        hero.Gold -= 15;
                        hero.CurrentHealth = hero.MaxHealth;
                        hero.CurrentMagic = hero.MaxMagic;
                        Console.WriteLine(@"
You wake up in your villages inn after a serious fight.
You have been revived for 15 gold");
                        BattleHelper.Anykey();
                        hero.isAlive = true;
                        bool b = false;
                        MainGame main = new MainGame(b, hero);
                        break;
                    }
                }

                Console.WriteLine("{0} is facing:", hero.Identifier);
                foreach (Characters monster in monsters)
                {
                    if (monster.Identifier == "Error Macro") Console.ForegroundColor = ConsoleColor.Red;
                    if (monster.isAlive)
                    {
                        Console.WriteLine("{0}", monster.Identifier);
                    }
                }
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine();

                BattleHelper.PrintBattleHeader();                
                BattleHelper.PrintBattleStatusBreaker();
                BattleHelper.PrintStatus(hero);

                foreach (Characters monster in monsters)
                {
                    if (monster.isAlive) BattleHelper.PrintStatus(monster);
                }

                userspellchoice = null;
                userchoice = BattleHelper.PrintChoice(hero);

                Console.WriteLine();
                BattleHelper.CheckDefence(userchoice, hero);
                if (userchoice == "s" || userchoice == "S") userspellchoice = BattleHelper.PrintSpells(hero);
                if (userchoice != "d" && userchoice != "p" && !(userchoice == "s" && userspellchoice == "h")) targetmonster = BattleHelper.ChooseTarget(monsters, userspellchoice);
                if (userchoice == "f") userchoice = "a";

                BattleHelper.ProcessChoice(userchoice, hero, monsters[targetmonster], userspellchoice);

                foreach (Characters monster in monsters)
                {
                    monster.isAlive = BattleHelper.CheckHealth(monster.CurrentHealth);
                    if (monster.isAlive == true)
                    {
                        monsterchoice = monster.AI();
                        BattleHelper.CheckDefence(monsterchoice, monster);
                        if (monsterchoice == "S" || monsterchoice == "s")
                        {
                            monsterspellchoice = monster.SpellAI();
                        }
                        
                        BattleHelper.ProcessChoice(monsterchoice, monster, hero, monsterspellchoice);
                    }
                }
                amonsterleft = BattleHelper.CheckMonsters(monsters, hero);
                hero.isAlive = BattleHelper.CheckHealth(hero.CurrentHealth);
                Console.WriteLine();
                BattleHelper.Anykey();
            }
            while (hero.isAlive == true && amonsterleft == true);
            
            if (hero.isAlive)
            {
                int itemSelect = OutputHelper.rand.Next(100);
                if (itemSelect <= 19 + (hero.Level / 2)) BattleHelper.RandomItem(hero);
            }
        }





    }
}



