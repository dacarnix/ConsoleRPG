﻿using System;
using System.Collections.Generic;

namespace RPG
{
    class NewVillageMain
    {
        List<Characters> Monster;
        Battle battle;
        string choice;
        bool Fight;
        public NewVillageMain(Hero hero)
        {
            Console.Clear();
            do
            {
                BattleHelper.HeroDeathVoid(hero);
                hero.secretVillage = true;
               
                Console.WriteLine(@"Welcome to the secret Village, {0}

You have {1}/{2}hp, {3}/{4}mp and {5} gold.

To return to your home village, you must find your way back via Roaming
",
                hero.Identifier, hero.CurrentHealth, hero.MaxHealth, hero.CurrentMagic, hero.MaxMagic, hero.Gold);
                Console.Write(@"What would you like to do?
_____________________________
(F)ight 
(R)oam
(S)tore
(I)nn (rest)
Eq(U)ip Items
L(E)vel Up Stats{0}
s(A)ve
(T)alk to the Owner
(V)iew stats and items
(Q)uit
_____________________________
", hero.SkillPoints > 0 ? " - " + hero.SkillPoints + " point to use" : "");
                choice = Console.ReadLine().ToLower();
                switch (choice)
                {
                    case "s":
                        Shop store = new Shop(hero);
                        break;
                    case "i":
                        Inn.Sleep(hero);
                        break;
                    case "v":
                        View.PrintStats(hero);
                        break;
                    case "e":
                        hero = BattleHelper.Leveling(hero);
                        break;
                    case "a":
                        data.Save(hero);
                        break;
                    case "u":
                        EquipItems.Equip(hero);
                        break;
                    case "r":
                        RoamingMain main = new RoamingMain(hero);
                        break;
                    case "t":
                        Console.Clear();
                        Console.WriteLine(@"3 bosses remain undefeated here
You must defeat them in order they appear or they will not accept your challenge...

In this village there also lurks a mysterious warrior, to fight him, you must type the answer
in the fight menu to this riddle where you can select the other bosses.
What is caused from 99! An _____ ?

(Please type this answer in the (F)ight menu, not here.)
");
                        BattleHelper.Anykey();
                        break;
                    case "f":
                        #region Fight
                        Console.Clear();
                            Console.Write(@"Which monster do you want to fight?
_________________________
1. Roshan
2. Kongor
3. Reptile
4. Final Boss

(You may type the answer to the owners riddle here)

(B)ack
_________________________");
                            Console.WriteLine();
                            choice = Console.ReadLine();
                            Monster = new List<Characters>();
                            if (choice == "1")
                            {
                                Monster.Add(new Roshan());
                                Fight = true;
                            }
                            else if (choice == "2")
                            {
                                if (hero.Boss1Defeated == true)
                                {
                                    Monster.Add(new Kongor());
                                    Fight = true;
                                }
                                else
                                {
                                    Console.WriteLine("You must defeat the first boss first!");
                                    BattleHelper.Anykey();
                                    break;
                                }
                            }
                            else if (choice == "3")
                            {
                                if (hero.Boss2Defeated == true)
                                {
                                    Monster.Add(new Reptile());
                                    Fight = true;
                                }
                                else
                                {
                                    Console.WriteLine("You must defeat the second boss first!");
                                    BattleHelper.Anykey();
                                    break;
                                }
                            }
                            else if (choice == "4")
                            {
                                if (hero.Boss3Defeated == true)
                                {
                                    Monster.Add(new Liu_King(hero));
                                    Fight = true;
                                }
                                else
                                {
                                    Console.WriteLine("You must defeat the third boss first!");
                                    BattleHelper.Anykey();
                                    break;
                                }
                            }
                            else if (choice.ToLower() == "error" || choice.ToLower() == "math error")
                            {
                                Monster.Add(new ErrorMacro());
                                Fight = true;
                                Console.WriteLine(@"You have found the easter egg!");
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.Write("Error Macro");
                                Console.Write(": We are many, you are but one");
                                Console.WriteLine();
                                Console.ForegroundColor = ConsoleColor.White;
                                BattleHelper.Anykey();
                            }
                            else if (choice == "B" || choice == "b") break;
                            else
                            {
                                Console.WriteLine("I didnt recognise that");
                                BattleHelper.Anykey();
                                break;
                            }
                            if (Fight == true)
                            {
                                Console.Clear();
                                battle = new Battle(hero, Monster);
                                if (hero.CurrentHealth <= 0)
                                {
                                    break;
                                }
                                else if (hero.fled == false)
                                {
                                    int gold = 0;
                                    int experience = 0;
                                    foreach (Characters monster in Monster)
                                    {
                                        if (monster.fled == false)
                                        {
                                            experience += monster.Experience;
                                            gold += (monster.Gold + hero.GoldBuff);
                                        }
                                    }
                                    Console.WriteLine("{0} gets {1} gold and {2} experience"
                                        , hero.Identifier, gold, experience);
                                    hero.Experience += experience;
                                    hero.Gold += gold;
                                    BattleHelper.expCheck(hero);
                                    Monster.Clear();
                                    BattleHelper.Anykey();
                                }
                                else hero.fled = false;
                                Fight = false;
                                break;
                            }
                            Fight = false;
                            break;
                        #endregion
                    case "q":
                            Menu m = new Menu();
                        break;
                        
                }
                Console.Clear();
            }
            while (choice != "q");
            
        }
    }
}
