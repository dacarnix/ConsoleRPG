﻿using System;
using System.IO;
using System.Xml;

namespace RPG
{
    class Menu
    {
        string choice;
        public Menu()
        {
            //char[] c = new char[1];
            //Console.WriteLine(c[3].ToString());
            Console.ForegroundColor = ConsoleColor.White;
            Console.Title = "Andrew's Game 1.2 Update";
            Console.Clear();
            do
            {
                Console.WriteLine(@"Welcome to Andrew's Game!
__________________________
What would you like to do?
(P)lay
(H)ow To Play
(C)redits
Ch(A)ngelog
(L)eave
__________________________");
                choice = Console.ReadLine().ToLower();
                switch (choice)
                {
                    case "p":
                        Console.WriteLine("Have fun!");
                        BattleHelper.Anykey();
                        #region StartLoad
                        string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                        var basePath = Path.Combine(path, data.folderName + Path.DirectorySeparatorChar);
                        if (!Directory.Exists(basePath)) Directory.CreateDirectory(basePath);

                        var fullPath = Path.Combine(basePath, data.saveFileName);

                        string heroName;
                        bool Load = false;
                        Hero myhero = new Hero();

                        Console.WriteLine("Why Hello there :D");
                        if (File.Exists(fullPath))
                        {
                            try
                            {
                                XmlDocument xDoc = new XmlDocument();
                                xDoc.Load(fullPath);
                                heroName = xDoc.SelectSingleNode("Hero/Identifier").InnerText.ToString();
                                if (heroName != "")
                                {
                                    while (true)
                                    {
                                        Console.WriteLine(@"
Would you like to continue the hero save: {0}?
(Y)es 
(N)o", heroName);
                                        var input = Console.ReadLine().ToLower();
                                        if (input == "Y" || input == "y" || input == "Yes" || input == "yes")
                                        {
                                            Load = true;
                                            break;
                                        }
                                        else if (heroName == "N" || input == "n" || input == "No" || input == "no")
                                        {
                                            Hero.Initialize(myhero);
                                            break;
                                        }
                                        else
                                        {
                                            Console.WriteLine("That is not a valid option");
                                        }
                                    }
                                }
                                else Hero.Initialize(myhero);
                            }
                            catch
                            {
                                Hero.Initialize(myhero);
                            }
                        } else
                        {
                            Hero.Initialize(myhero);
                        }
#endregion
                      
                        MainGame main = new MainGame(Load, myhero);
                        break;
                    case "h":
                        #region HowToPlay
                        Console.Clear();
                        Console.WriteLine(@"
Welcome to this game ;o
The objective of the game is to level up
from killing monsters around your home village.
Once you've gotten strong enough, and finally killed the Rogue Commander
which has been plagueing your town for centuries, you
can go roaming. From here you need to explore enough outside your
town until you find another village and kill the four bosses waiting for you there.

Page 1/4

");

                        BattleHelper.Anykey();
                        Console.Clear();
                        Console.WriteLine(@"
When you are fighting, you have various attack styles.
Each turn you can do one of the following:
- Attack
- Defend (this lowers any incoming damage that turn, and gives
  a boost to your attack the following turn).
- Use a spell, if you have enough mana (or called MP, for mana points)
- Use a potion, if you have any, to restore health (called HP) or mana.
- Flee from the battle, running away.

You can not flee from bosses so make sure you save regularly.
If you die, you will respawn at your last town if you have at least 10 or
15 gold (depending if you are roaming or not).

Page 2/4

");
                        BattleHelper.Anykey();
                        Console.Clear();
                        Console.WriteLine(@"
For leveling, each attribute helps you in a different way.

Strength - Adds 4 health and 6 attack damage per level
Agility - Adds 4 defense and increases critical strike chance per level
Intelligence - Adds 10 max mana and improves magic damage per level

When asked to select an option, type the letter in the ()
eg. to use the (F)ight option,
You would type F or f.

Page 3/4

");
                        BattleHelper.Anykey();
                        Console.Clear();
                        Console.WriteLine(@"
You can obtain items in this game, either through
buying them in the store with gold or by killing enemies. 
Some examples of items are:

Leather chestplate - provides armour
Dirty dagger - Small damage increase
Awesome sword - Massive damage and +4 intelligence
Impowering sword - High damage and  +30 max health

You can see that some items also give other bonuses
rather than only boosting your attack or defense, such as adding intelligence.

Page 4/4

");
                        BattleHelper.Anykey();
                        #endregion
                        continue;
                    case "c":
                        #region Credits
                        Console.Clear();
                        Console.WriteLine(@"Main developer - Thickshake
Beta testers - Leachy, ltman, leetpro, Nonliquet, ChandyMan, Warmmilk.
Ideas - ltman, Nonliquet
First person to finish - ChandyMan (pre game balance too!)
");
                        BattleHelper.Anykey();
                        continue;
                    #endregion
                    case "a":
                        #region Changelog
                        Console.Clear();
                        Console.WriteLine(@"Original game from 2012, version 1.2+ was made ~April 3rd 2022
by touching up some critical bugs and game balances.

V1.2

Balances
- Balanced how critical strike works for enemies with high agility (it would always crit)
- Slightly nerfed the `Troll` enemy
- Increased minimum damage when defender has high defense, it often would hit 0
- Does not still attempt to cast a spell (and use your turn) if you have insufficient mana
- Made killing the easter egg give gold and experience (previously was 0)
- Strength now grants +4 max health per level (this was not working previously, it granted none)
- Decreased attack gain from 8 to 6 per strength level (because of health buff, too)
- Increased the amount of defense agility grants per level from 3 to 5
- Decreased the increase of damage of fireball and iceball based on intelligence
- Slightly improved the amount healed from 3 to 5 extra per intelligence level for `Heal` spell
- Mana potion now restores 100
- Reduce cost of Manly Hammer from 700(?!) to 200
- Increased base agility from 6 to 7
- Nerfed virtually all enemies Agility levels, they were not balanced properly and only used to determine critical strike chance
- Made item drops more useful at higher levels

Additions / Removals
- Re-worked equipping armour, so you don't have to unequip everything before changing items
- Removed the (L)oad option from the main menu

Visual
- Visual improvements, overhauled the `View` menu and improved the data presented in `Inn`
- Improved the battle menu to remove pointless information and make it more concise
- Other small label improvements
- Overhauled `Potion` and `Spell` choose menu during a battle
- Overhauled how items are shown, now include their buffs
- Show skill points if available to use in the menu
- Improved the `Credits` menu
- Removed reference to developer name when using a potion

Bugs
- Fixed corrupt saved files from occuring
- Fixed bug where you can't use a spell then choose to attack when fighting multiple enemies
- Massively improved reliability of random variables by using one shared static `Random` object
- Fixed ability to go back from the roaming menu correctly
- Added option to go back from the spell menu
- Added option to go back from the potion menu, this was especially broken if you had no potions
- Added option to go back from the `(F)ight` menu
- Added option to go back from the potion buy menu
- Properly target the last remaining enemy if there is only one (from initially multiple)
- Fixed not being able to die in a `Roaming` fight
- Removed ability to stumble into the `Secret Village` while on the first turn of roaming
- Fixed while roaming asking for what target to attack while using `Heal` or a potion
- Fixed case where you could get multiple of the same weapon from killing bosses
- Fixed spells not always showing effect if your mana went to 0
- Fixed some items not being equipable
- Fixed ability to bypass loading question screen with invalid input

Pre 1.2
Lost in the abyss :'(
If you'd like to play pre 1.2, check out an earlier commit <3

Known issues
- It is not possible to return to the home village once you found the secret village,
  the only known workaround is dying in roaming and respawning in the home village.
  Selecting the village return every 3 rounds may work, too.
- Death detection in roaming (being <0 HP) may still be buggy
- Fireball should hit multiple enemies but that logic is still missing
");
                        BattleHelper.Anykey();
                        continue;
                    #endregion

                    case "l":
                        Console.WriteLine("Take care!");
                        BattleHelper.Anykey();
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Please enter a correct letter");
                        BattleHelper.Anykey();
                        break;
                }
            } while (choice != "l");

        }

    }
}
